package socialnetwork;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.mindrot.jbcrypt.BCrypt;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.file.FriendRequestFile;
import socialnetwork.repository.file.FriendshipFile;
import socialnetwork.repository.file.MessageFile;
import socialnetwork.repository.file.UserFile;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.service.*;
import socialnetwork.ui.UI;
import socialnetwork.ui.utils.Reader;
import utils.Constants;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Main {
    public static void main(String[] args) throws IOException {
//        //String fileName1=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users").replace("%20"," ");
//        //String fileName="data/users.csv";
//
//        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
//        final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
//        final String password= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

//       UserFile userRepository = new UserFile(fileName1
////                , new UserValidator());
////
////        String fileName2=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendships").replace("%20"," ");
////        FriendshipFile prietenieRepository = new FriendshipFile(fileName2, new FriendshipValidator());
////        String fileName3=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.messages").replace("%20"," ");
////        MessageFile messageRepository = new MessageFile(fileName3, new MessageValidator(), userRepository);
////        String fileName4=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendrequests").replace("%20"," ");
////        FriendRequestFile friendRequestFile = new FriendRequestFile(fileName4,new FriendRequestValidator());
//
//
//
//          FriendshipRepositoryDB friendshipRepository = new FriendshipRepositoryDB(url,username,password,new FriendshipValidator());
//          UserRepositoryDB userRepository = new UserRepositoryDB(url,username,password,new UserValidator());
//          EventRepositoryDB eventRepositoryDB = new EventRepositoryDB(url,username,password,new EventValidator());
//
//          MessageRepositoryDB messageRepository = new MessageRepositoryDB(url,username,password,new MessageValidator());
//          FriendRequestRepositoryDB friendRequestRepository = new FriendRequestRepositoryDB(url,username,password,new FriendRequestValidator());
////
//        UserService userService = new UserService(userRepository);
//        FriendshipService friendshipService = new FriendshipService(friendshipRepository);
//        MessageService messageService = new MessageService(messageRepository);
//        FriendRequestService friendRequestService = new FriendRequestService(friendRequestRepository);
//        ComunityService comunityService = new ComunityService(friendshipService, userService, messageService, friendRequestService);
//        UI ui = new UI(comunityService);
//        try {
//            //String s = Reader.readString();
//            //System.out.println("stringul citit de mine este: "+s);
//            ui.run();
//        }catch(IOException io) {
//            System.out.println(io.getMessage());
//        }
//
//    }

        //EventRepositoryDB eventRepositoryDB = new EventRepositoryDB(url,username,password, new EventValidator());


//        Pageable pageable = new PageableImplementation(2,3);
//        Page<User> currentPage = userRepository.findAll(pageable);
//        Stream<User> users = currentPage.getContent();
//        System.out.println("Prima pagina");
//        users.forEach(System.out::println);
//        currentPage = userRepository.findAll(currentPage.nextPageable());
//        users = currentPage.getContent();
//        System.out.println("A doua pagina");
//        users.forEach(System.out::println);

//        Pageable pageable = new PageableImplementation(1,2);
//        Page<Event> currentPage = eventRepositoryDB.findAll(pageable);
//        Stream<Event> events = currentPage.getContent();
//        System.out.println("Prima pagina");
//        events.forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = eventRepositoryDB.findAll(currentPage.nextPageable());
//        events = currentPage.getContent();
//        events.forEach(System.out::println);

//        Pageable pageable = new PageableImplementation(1,1);
//        Page<Friendship> currentPage = friendshipRepository.getUserFriends(pageable,2L);
//        System.out.println("Prima pagina");
//        currentPage.getContent().forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = friendshipRepository.getUserFriends(currentPage.nextPageable(),2L);
//        currentPage.getContent().forEach(System.out::println);

//        Pageable pageable = new PageableImplementation(1,1);
//        Page<Message> currentPage = messageRepository.userPagingReceivedMessages(pageable,3L);
//        Stream<Message> messages = currentPage.getContent();
//        System.out.println("Prima pagina");
//        messages.forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = messageRepository.userPagingReceivedMessages(currentPage.nextPageable(),3L);
//        messages = currentPage.getContent();
//        messages.forEach(System.out::println);

//        Pageable pageable = new PageableImplementation(1,1);
//        Page<FriendRequest> currentPage = friendRequestRepository.usersPagingFriendRequests(pageable,3L);
//        System.out.println("Prima pagina");
//        currentPage.getContent().forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = friendRequestRepository.usersPagingFriendRequests(currentPage.nextPageable(), 3L);
//        currentPage.getContent().forEach(System.out::println);


//        Pageable pageable = new PageableImplementation(1,1);
//        Page<Message> currentPage = messageRepository.userPagingSentMessages(pageable,1L);
//        Stream<Message> messages = currentPage.getContent();
//        System.out.println("Prima pagina");
//        messages.forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = messageRepository.userPagingSentMessages(currentPage.nextPageable(),1L);
//        messages = currentPage.getContent();
//        messages.forEach(System.out::println);


//        List<User> userList = StreamSupport.stream(userRepository.findAll().spliterator(),false).collect(Collectors.toList());
//        for(User u: userList){
//            u.setPassword(BCrypt.hashpw("parolacriptata",u.getSalt()));
//            userRepository.update(u);
//        }
//        User user = userRepository.findOne(1L);
//        if(!BCrypt.hashpw("parolacriptata",user.getSalt()).equals(user.getPassword()))
//            System.out.println("INCORECT");
//        else
//            System.out.println("CORECT");
//        String salt = BCrypt.gensalt();
//        String parola1 = BCrypt.hashpw("parola",salt);
//        String parola2 = BCrypt.hashpw("parola",salt);
//        if(parola1.equals(parola2))
//            System.out.println("CORECT");
//        else
//            System.out.println("INCORECT");

//        Page<User> page = userRepository.findAll(new PageableImplementation(2,3));
//        List<User> users = page.getContent().collect(Collectors.toList());
//        System.out.println(users);


//
//        LocalDateTime now = LocalDateTime.now();
//        System.out.println(now);
//        LocalDateTime eventDate = LocalDateTime.of(2020,12,27,12,34);
//        System.out.println(eventDate);
//        System.out.println(now.isAfter(eventDate));
//        long duration = Duration.between(now,eventDate).toDays();
//        System.out.println(duration);

//        NotificationsRepositoryDB notificationsRepositoryDB = new NotificationsRepositoryDB(url,username,password,eventRepositoryDB,userRepository);

//        Pageable pageable = new PageableImplementation(1,3);
//        Page<Notification> currentPage = notificationsRepositoryDB.findAll(pageable,1L);
//        Stream<Notification> messages = currentPage.getContent();
//        System.out.println("Prima pagina");
//        messages.forEach(System.out::println);
//        System.out.println("A doua pagina");
//        currentPage = notificationsRepositoryDB.findAll(currentPage.nextPageable(),1L);
//        messages = currentPage.getContent();
//        messages.forEach(System.out::println);
//        System.out.println("A treia pagina");
//        currentPage = notificationsRepositoryDB.findAll(currentPage.nextPageable(),1L);
//        messages = currentPage.getContent();
//        messages.forEach(System.out::println);
//        System.out.println("A patra pagina");
//        currentPage = notificationsRepositoryDB.findAll(currentPage.nextPageable(),1L);
//        messages = currentPage.getContent();
//        messages.forEach(System.out::println);

     //   messageRepository.userPagingSentMessages(new PageableImplementation(1,2),1L).getContent().forEach(System.out::println);


        MainFX.main(args);

    }
}


