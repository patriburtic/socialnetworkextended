package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.utils.Status;
import socialnetwork.repository.database.FriendRequestRepositoryDB;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.service.exceptions.ComunityException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FriendRequestService {
    private FriendRequestRepositoryDB repo;

    public FriendRequestService(FriendRequestRepositoryDB repo) {
        this.repo = repo;
    }

    /**
     *this method sends a FriendReuest from User u1 to User u2
     * @param u1 - sender
     * @param u2 -receiver
     * @return a FriendRequest if successful, null otherwise
     */
    public FriendRequest sendFriendRequest(User u1, User u2){
        Tuple<Long, Long> id = new Tuple<>(u1.getId(), u2.getId());
        Tuple<Long, Long> id1 = new Tuple<>(u2.getId(), u1.getId());
        if((findOne(id1) != null && findOne(id1).getStatus() != Status.APPROVED) || (findOne((id)) != null && findOne(id).getStatus() != Status.APPROVED)){
            throw new ComunityException("Exista deja o cerere de prietenie trimisa intre acesti useri \n care asteapta sa fie acceptata sau a fost respinsa.");
        }
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setId(id);
        return repo.save(friendRequest);
    }

    /**
     *
     * @return all friend requests
     */
    public Iterable<FriendRequest> findAll(){
        return repo.findAll();
    }

    public Iterable<FriendRequest> findAllPaging(int pageNumber, int pageSize){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<FriendRequest> currentPage = repo.findAll(pageable);
        Stream<FriendRequest> friendRequestStream = currentPage.getContent();
        return friendRequestStream.collect(Collectors.toList());
    }

    /**
     *
     * @param id - id of a friend request
     * @return the friend request with the given id
     */
    public FriendRequest findOne(Tuple<Long,Long> id){
        return repo.findOne(id);
    }

    /**
     * this method respond to a friend request based on users's choice
     * @param id - id of the friend request to respond to
     * @param response - user's choice
     *                 1- accept
     *                 2- reject
     * @return the updated form of friend request if successful, null otherwise
     */
    public FriendRequest respondToFriendRequest(Tuple<Long,Long> id, int response){
        FriendRequest friendRequest = repo.findOne(id);
        repo.delete(id);
        if(response == 1)
            friendRequest.setStatus(Status.APPROVED);
        else if(response == 2)
            friendRequest.setStatus(Status.REJECTED);
        else
            throw new ComunityException("Invalid status!");
        return repo.save(friendRequest);
    }

    /**
     * deletes a friend request based on id
     * @param id - the id of the friend request to be deleted
     * @return the deleted fR, null otherwise
     */
    public FriendRequest deleteFriendRequest(Tuple<Long,Long> id){
        return repo.delete(id);
    }

    /**
     *
     * @param id - id of the user
     * @return a list of pending friend request of the user
     */

    public List<FriendRequest> loadPendingRequests(long id){
        List<FriendRequest> myFriendRequests = new ArrayList<>();
        for (FriendRequest friendRequest : findAll()) {
            if(friendRequest.getId().getRight() == id && friendRequest.getStatus() == Status.PENDING)
                myFriendRequests.add(friendRequest);
        }
        return myFriendRequests;
    }


    public Stream<FriendRequest> getAllPaging(int index, int limit, User user){
        Pageable pageable = new PageableImplementation(index,limit);
        Page<FriendRequest> page = repo.usersPagingFriendRequests(pageable, user.getId());
        return page.getContent();
    }

    public int countFriendRequests(User user){
        Pageable pageable = new PageableImplementation(1,100);
        Page<FriendRequest> page = repo.usersPagingFriendRequests(pageable, user.getId());
        int size = (int) page.getContent().count();
        page = repo.usersPagingFriendRequests(page.nextPageable(), user.getId());
        while(page.getContent().count() != 0){
            page = repo.usersPagingFriendRequests(page.nextPageable(), user.getId());
            size += (int) page.getContent().count();
        }
        return size;
    }



}
