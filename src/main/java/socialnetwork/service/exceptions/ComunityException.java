package socialnetwork.service.exceptions;

public class ComunityException extends RuntimeException{
    public ComunityException() {
    }

    public ComunityException(String message) {
        super(message);
    }

    public ComunityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ComunityException(Throwable cause) {
        super(cause);
    }

    public ComunityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

