package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.repository.database.EventRepositoryDB;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class EventService {
    private final EventRepositoryDB repo;

    public EventService(EventRepositoryDB repo) {
        this.repo = repo;
    }

    public Iterable<Event> findAll(){return repo.findAll();}

    public List<Event> findAllPaging(int pageNumber, int pageSize){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<Event> currentPage = repo.findAll(pageable);
        Stream<Event> eventStream = currentPage.getContent();
        return eventStream.
                sorted((x,y)-> (int) Duration.between(x.getStartDate(),y.getStartDate()).toDays())
                        .collect(Collectors.toList());
    }

    public List<Event> usersParticipatingEvent(int pageNumber, int pageSize, Long idUser){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<Event> currentPage = repo.usersParticipatingEvents(pageable,idUser);
        Stream<Event> eventStream = currentPage.getContent();
        return eventStream.
                sorted((x,y)-> (int) Duration.between(x.getStartDate(),y.getStartDate()).toDays())
                .collect(Collectors.toList());
    }


    public Event findOne(Long id){return repo.findOne(id);}

    public Event createNewEvent(String title, String description, String location, LocalDateTime startDate, LocalDateTime finishDate, User user){
        Event event = new Event(title, description, location, startDate, finishDate);
        event.setId(findNextID());
        event.setCreator(user);
        return repo.save(event);
    }

    public Event deleteEvent(Long id){
        return repo.delete(id);
    }

    public Event addParticipantToEvent(Long idEvent, Long idParticipant){
        return repo.addParticipant(idEvent, idParticipant);
    }

    public Event removeParticipantFromEvent(Long idEvent, Long idParticipant){
        return repo.deleteParticipant(idEvent,idParticipant);
    }

    public Event updateUserChoice(Long idEvent, Long idParticipant, Boolean notificationChoice){
        return repo.updateParticipant(idEvent, idParticipant, notificationChoice);
    }

    public Iterable<Event> getUserEvents(User user){
        return StreamSupport.stream(findAll().spliterator(),false)
                .filter(x-> x.getParticipants().stream()
                        .anyMatch(y -> y.getLeft().equals(user) && y.getRight().equals(true)))
                .collect(Collectors.toList());
    }

    public int countMyEvents(User user){
        return (int) StreamSupport.stream(findAll().spliterator(),false)
                .filter(x-> x.getParticipants().stream()
                        .anyMatch(y -> y.getLeft().equals(user)))
                        .count();
    }

    private long findNextID(){
        long max = 0;
        for(Event e: findAll()){
            if(e.getId() > max)
                max = e.getId();
        }
        return max+1;
    }




}
