package socialnetwork.service;


import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendshipRepositoryDB;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FriendshipService {
    private FriendshipRepositoryDB repo;

    public FriendshipService(FriendshipRepositoryDB repo) {
        this.repo = repo;
    }

    /**
     *
     * @param friendship
     * @return null if the entity was saved or the entity that has the same ID
     */
    public Friendship addFriendship(Friendship friendship){
        Friendship savedFr = repo.save(friendship);
        return savedFr;
    }

    /**
     *
     * @param id
     * @return the deleted friendship
     * @return null if there isn't a friendship having this id
     */
    public Friendship deleteFriendship(Tuple<Long,Long> id){
        Friendship friendship = repo.delete(id);
        return friendship;
    }

    /**
     *
     * @return all friendships
     */
    public Iterable<Friendship> getAll(){
        return repo.findAll();
    }

    public Iterable<Friendship> findAllPaging(int pageNumber, int pageSize){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<Friendship> currentPage = repo.findAll(pageable);
        Stream<Friendship> friendshipStream = currentPage.getContent();
        return friendshipStream.collect(Collectors.toList());
    }

    public Friendship findOne(Tuple<Long,Long> id){
        return repo.findOne(id);
    }

    public Stream<Friendship> getUserFriendship(int index, int limit, User user){
        return repo.getUserFriends(new PageableImplementation(index,limit), user.getId()).getContent();
    }
}
