package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.repository.database.UserRepositoryDB;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserService {
    private UserRepositoryDB repo;

    public UserService(UserRepositoryDB repo) {
        this.repo = repo;
    }

    /**
     *
     * @param user
     * @return null if the entity was saved or the entity that has the same ID
     */
    public User addUser(User user) {
        user.setId(findNextID());
        User savedUser = repo.save(user);
        return savedUser;
    }

    /**
     *
     * @param id
     * @return the deleted user
     * @return null if there isn't a user with this id
     */
    public User deleteUser(Long id){
        return repo.delete(id);

    }

    /**
     *
     * @param id
     * @return the entity with given id
     */

    public User findOne(Long id){
        return repo.findOne(id);
    }

    /**
     *
     * @return all users
     */
    public Iterable<User> getAll(){

        return repo.findAll();
    }

    public List<User> findAllPaging(int pageNumber, int pageSize){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<User> currentPage = repo.findAll(pageable);
        Stream<User> users = currentPage.getContent();
        return users.collect(Collectors.toList());
    }

    /**
     * private method that manages the IDs
     * @return next available ID
     */
    private long findNextID(){
        long max = 0;
        for(User u: getAll()){
            if(u.getId() > max)
                max = u.getId();
        }
        return max+1;
    }

    public User findByUsername(String username){
        return repo.findByUsername(username);
    }

    public boolean thisUsernameExists(String username){
        return repo.findByUsername(username) != null;
    }
}
