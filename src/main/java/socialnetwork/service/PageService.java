package socialnetwork.service;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.mindrot.jbcrypt.BCrypt;
import socialnetwork.Observer.Observable;
import socialnetwork.Observer.Observer;
import socialnetwork.domain.*;
import socialnetwork.domain.utils.Status;
import socialnetwork.domain.MessageDTO1;
import socialnetwork.service.exceptions.ComunityException;
import utils.Constants;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Stack;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PageService implements Observable {
    private Map<Long, List<Long>> friendships;
    private static HashMap<Long, Long> visited;
    private FriendshipService friendshipService;
    private MessageService messageService;
    private FriendRequestService friendRequestService;
    private UserService userService;
    private EventService eventService;
    private final NotificationService notificationService;
    private ConnectionService connectionService;
    private User currentUser;
    private boolean newNotification;

    public boolean isNewNotification() {
        return newNotification;
    }

    public void setNewNotification(boolean newNotification) {
        this.newNotification = newNotification;
    }

    /**
     *
     * @param friendshipService
     * @param userService
     * @param messageService
     * @param friendRequestService
     * @param eventService
     * @param notificationService
     * @param connectionService
     */
    public PageService(FriendshipService friendshipService, UserService userService,
                       MessageService messageService, FriendRequestService friendRequestService,
                       EventService eventService, NotificationService notificationService,
                       ConnectionService connectionService) {
        this.friendshipService = friendshipService;
        this.userService = userService;
        this.messageService = messageService;
        this.friendRequestService = friendRequestService;
        this.eventService = eventService;
        this.notificationService = notificationService;
        this.connectionService = connectionService;
        friendships = new HashMap<>();
        visited = new HashMap<>();
        newNotification = false;
        load();
    }

    /**
     *
     * @return prietenieService
     */

    public FriendshipService getFriendshipService() {
        return friendshipService;
    }

    public EventService getEventService() {
        return eventService;
    }

    /**
     *
     * @return utilizatorService
     */

    public UserService getUserService() {
        return userService;
    }

    public MessageService getMessageService() {
        return messageService;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    public FriendRequestService getFriendRequestService() {
        return friendRequestService;
    }

    /**
     * loads all users and friendhips creating a graph
     */


    public void load() {
        friendships.clear();
        Iterable<Friendship> friendshipIterable = friendshipService.getAll();
        Iterable<User> userIterable = userService.getAll();
        for(User u:userIterable){
            friendships.putIfAbsent(u.getId(),new ArrayList<>());
        }
        for(Friendship p:friendshipIterable){
            long first = p.getId().getLeft();
            long second = p.getId().getRight();
            friendships.get(first).add(second);
            friendships.get(second).add(first);
        }

        setVisited();

    }

    /**
     *
     * @param id
     * delete all inactive frienships in the comunity
     * if a user no longer exists, all his friendships are deleted
     */
    public void deleteInactiveFriendshipsMessagesFriendRequests(long id)  {
        for(Friendship friendship: friendshipService.getAll()){
            if(friendship.getId().getLeft() == id || friendship.getId().getRight() == id)
                friendshipService.deleteFriendship(friendship.getId());
        }
        for(FriendRequest friendRequest: friendRequestService.findAll()){
            if(friendRequest.getId().getLeft() == id || friendRequest.getId().getRight() == id)
                friendRequestService.deleteFriendRequest(friendRequest.getId());
        }
        for(Message message: messageService.getAll()){
            if(message.getFrom().getId() == id || message.getTo().contains(userService.findOne(id))){
                messageService.deleteMessage(message.getId());
            }
        }

        for(Event event: eventService.findAll()){
            if(event.getParticipantsOnly().contains(userService.findOne(id))){
                removeParticipantFromEvent(event.getId(),id);
            }
            if(event.getCreator().getId().equals(id))
                eventService.deleteEvent(event.getId());
        }

        for(Notification notification:notificationService.getAllUserNotifications(id)){
            notificationService.deleteNotification(notification.getId());
        }

    }

    /**
     *
     * @param user
     * DFS algorithm
     */
    private void DFS(long user)
    {
        visited.put(user, (long) 1);
        for(Long friend : friendships.get(user))
        {
            if(visited.get(friend) == 0){
                DFS(friend);
            }
        }
    }

    /**
     *
     * @return the number of comunities
     */
    public int nrComunitati() {
        setVisited();
        int comunities = 0;
        for (Long user : visited.keySet()) {
            if (visited.get(user) == 0) {
                DFS(user);
                comunities++;
            }
        }
        return comunities;
    }

    /**
     * a private method that sets the visited map values to 0
     */
    private void setVisited(){
        for(User u: userService.getAll())
            visited.put(u.getId(), (long) 0);
    }

    /**
     * Another DFS which returns a list of user's connected friends
     * uses a Stack
     * @param id the user's id
     * @return the list of the user's connected friends
     */
    public ArrayList<Long> DFS2(Long id)
    {
        Stack<Long> st = new Stack<>();
        ArrayList<Long> ar = new ArrayList<>();

        st.push(id);
        while(!st.isEmpty())
        {
            Long el = st.pop();
            // if is not in visited
            if(visited.get(el) == 0)
            {
                visited.put(el,(long)1);
                ar.add(el);
            }

            for(Long vertex: friendships.get(el))
                if(visited.get(vertex) == 0)
                    st.push(vertex);

        }
        return ar;
    }

    /**
     *
     * @return the most sociable comunity
     */
    public ArrayList<Long> most_sociable_comunity()
    {
        setVisited();
        ArrayList<Long> comunity = new ArrayList<>();
        ArrayList<Long> afterdfs= new ArrayList<>();


        for(HashMap.Entry<Long,Long> entry: visited.entrySet())
        {
            if (entry.getValue() == 0)
            {
                afterdfs = DFS2(entry.getKey());
                if(afterdfs.size()>comunity.size())
                    comunity = afterdfs;
            }
        }
        return comunity;
    }

    public List<User> getUserFriends(User u) {
        Iterable<User> userIterable = userService.getAll();
        List<User> friends = new ArrayList<>();
        for (Long id : friendships.get(u.getId())) {
            Friendship pr = friendshipService.findOne(new Tuple<Long, Long>(u.getId(), id));
                if (pr != null)
                    friends.add(userService.findOne(id));
                else {
                    pr = friendshipService.findOne(new Tuple<Long, Long>(id, u.getId()));
                    friends.add(userService.findOne(id));
                }
            }
        return friends;
    }

    public List<FriendDTO> getUserFriendsDTO(User u){
        List<FriendDTO> friends = new ArrayList<>();
        for (Long id : friendships.get(u.getId())) {
            Friendship pr = friendshipService.findOne(new Tuple<Long, Long>(id, u.getId()));
            User friend = userService.findOne(id);
            FriendDTO friendDTO = new FriendDTO(friend.getFirstName(),friend.getLastName(),friend.getUsername(),pr.getDate().format(Constants.DATE_TIME_FORMATTER));
            friends.add(friendDTO);
        }
        return friends;

    }

    public List<FriendDTO> getUserFriends(int index, int limit, User user){
        return friendshipService.getUserFriendship(index, limit, user)
                .map(x->{
                    if(!x.getId().getLeft().equals(user.getId())){
                        User friend = userService.findOne(x.getId().getLeft());
                        return new FriendDTO(friend.getFirstName(),friend.getLastName(),friend.getUsername(),x.getDate().format(Constants.DATE_FORMATTER));
                    }
                    else{
                        User friend = userService.findOne(x.getId().getRight());
                        return new FriendDTO(friend.getFirstName(),friend.getLastName(),friend.getUsername(),x.getDate().format(Constants.DATE_FORMATTER));
                    }
                })
                .collect(Collectors.toList());
    }


    public void addUser(String firstName, String lastName, String username, String password) {
        User user = new User(firstName, lastName, username, password);
        if(userService.thisUsernameExists(username))
            throw new ComunityException("Exista deja un utilizator cu acest username!\n");
        User saved = userService.addUser(user);
        if(saved != null)
            throw new ComunityException("User-ul nu a fost adaugat!");
        load();
    }

    public void deleteUser(long longId) {
        User search = userService.findOne(longId);
        if(search == null)
            throw new ComunityException("Nu exista un utilizator cu acest id.");
        deleteInactiveFriendshipsMessagesFriendRequests(longId);
        User deleted = userService.deleteUser(longId);
        if(deleted == null)
            throw new ComunityException("Nu s-a putut sterge acest utilizator.");
        load();
    }

    public void addFriendship(Long id1, Long id2) {
        User u1 = userService.findOne(id1);
        User u2 = userService.findOne(id2);
        if (u1 == null || u2 == null)
            throw new ComunityException("Imposibil de stabilit aceasta prietenie.");
        else {
            Tuple<Long, Long> tuple = new Tuple<>(id1, id2);
            Friendship friendship = new Friendship();
            friendship.setId(tuple);
            Friendship savedFr = friendshipService.addFriendship(friendship);
            if (savedFr != null)
                throw new ComunityException("ATENTIE! Exista deja o relatie de prietenie intre acesti useri.");
            load();
        }
    }

    public void deleteFriendship(long longId1, long longId2) {
        Tuple<Long, Long> tuple = new Tuple<>(longId1, longId2);
        Tuple<Long, Long> tuple1 = new Tuple<>(longId2, longId1);
        Friendship deletedFr = friendshipService.deleteFriendship(tuple);
        if(deletedFr == null){
            Friendship deletedFr1 = friendshipService.deleteFriendship(tuple1);
            if(deletedFr1 == null)
                throw new ComunityException("Nu exista un o prietenie intre acesti useri.");
            else {
                friendRequestService.deleteFriendRequest(tuple);
                load();
            }
        }
        else {
            friendRequestService.deleteFriendRequest(tuple);
            load();
        }
    }

    public List<String> showAllFriends(long longId1) {
        User u = userService.findOne(longId1);
        if(u == null)
            throw new ComunityException("Nu exista un user cu acest ID!\n");
        List<String> friends= getUserFriends(u).stream()
                .map(x -> {
                    Friendship friendship = getFriendshipService().findOne(new Tuple<>(x.getId(), u.getId()));
                    if (friendship != null) {
                        return x.getLastName() + "|"
                                + x.getFirstName() + "|" + friendship.getDate().getYear() + "-" + friendship.getDate().getMonth() + "-" + friendship.getDate().getDayOfMonth() + "\n";
                    } else {
                        Friendship friendship1 = getFriendshipService().findOne(new Tuple<>(u.getId(), x.getId()));
                        return x.getLastName() + "|"
                                + x.getFirstName() + "|" + friendship1.getDate().getYear() + "-" + friendship1.getDate().getMonth() + "-" + friendship1.getDate().getDayOfMonth() + "\n";
                    }
                })
        .collect(Collectors.toList());

        return friends;
    }

    public boolean userExists(Long id){
        User user = userService.findOne(id);
        return user != null;
    }

    public List<String> filterFriendsByMonth(long longId1, int monthInt) {
        User u = userService.findOne(longId1);
        if(u == null)
            throw new ComunityException("Nu exista acest user.");

            List<User> friends = getUserFriends(u);
         return friends.stream()
                    .filter(x-> {
                        Friendship friendship = friendshipService.findOne(new Tuple<>(x.getId(), u.getId()));
                        if(friendship != null)
                            return friendship.getDate().getMonthValue() == monthInt;
                        else{
                            Friendship friendship1 = friendshipService.findOne(new Tuple<>(u.getId(),x.getId()));
                            return friendship1.getDate().getMonthValue() == monthInt;
                        }
                    })
                    .map(x-> {
                        Friendship friendship = friendshipService.findOne(new Tuple<>(x.getId(), u.getId()));
                        if(friendship != null) {
                            return x.getLastName() + "|"
                                    + x.getFirstName() + "|" + friendship.getDate().getYear() + "-" + friendship.getDate().getMonth() + "-" + friendship.getDate().getDayOfMonth() + "\n";
                        }
                        else{
                            Friendship friendship1 = friendshipService.findOne(new Tuple<>(u.getId(),x.getId()));
                            return x.getLastName() + "|"
                                    + x.getFirstName() + "|" + friendship1.getDate().getYear() + "-" + friendship1.getDate().getMonth() + "-" + friendship1.getDate().getDayOfMonth() + "\n";
                        }
                    })
                    .collect(Collectors.toList());

    }

    public void sendMessage(User from, List<Long> to, String txt) {
        List<User> toUsers = new ArrayList<>();
        to.forEach(x -> toUsers.add(userService.findOne(x)));
        Message message = new Message(from,toUsers,txt);
        if(messageService.addMessage(message) != null)
            throw new ComunityException("Mesajul nu s-a putut trimite.");
    }

    public void replyToMessage(Message messageToReply, User from, String txt) {
        Message reply = new Message(txt);
        reply.setFrom(from);
        List<User> to = new ArrayList<>();
        to.add(messageToReply.getFrom());
        for(User u: messageToReply.getTo()){
            if(!u.getId().equals(from.getId()))
                to.add(u);
        }
        reply.setTo(to);
        if(messageService.reply(messageToReply,reply) != null)
            throw new ComunityException("Nu s-a putut raspunde la mesaj!");
        notifyObservers();
    }

    public void sendFriendRequest(User me, User friend) {
        Tuple<Long, Long> id1 = new Tuple<>(me.getId(),friend.getId());
        Tuple<Long, Long> id2 = new Tuple<>(friend.getId(),me.getId());
        if((friendshipService.findOne(id1) != null || friendshipService.findOne(id2) !=null))
                throw new ComunityException("Exista deja o relatie de prietenie intre voi.");
        FriendRequest friendRequest = friendRequestService.sendFriendRequest(me,friend);
        if(friendRequest != null)
            throw new ComunityException("Nu s-a putut trimite cererea de prietenie.");
    }

    public void respondToFriendRequest(Long myId, Long requestId, int choice) {
        Tuple<Long, Long> newFrId = new Tuple<Long, Long>(requestId, myId);
        switch (choice) {
            case 1:
                if (friendRequestService.respondToFriendRequest(newFrId, choice) == null) {

                    Friendship friendship = new Friendship();
                    friendship.setId(newFrId);
                    friendshipService.addFriendship(friendship);
                    load();

                } else
                    throw new ComunityException("Cererea de prietenie nu a fost acceptata cu succes.");
                break;
            case 2:
                if (friendRequestService.respondToFriendRequest(newFrId, choice) != null)
                    throw new ComunityException("Cererea de prietenie nu a fost refuzata cu succes.");
                break;
            default:
                throw new ComunityException("Ceva nu a mers bine.");
        }
    }


    public void logIn(String username, String password){
        User user = userService.findByUsername(username);
        if(user != null){

            if(!BCrypt.checkpw(password,user.getPassword())) {
                throw new ComunityException("Parola incorecta!\n");
            }

            else{
                currentUser = user;
                manageConnection();
            }

        }
        else
            throw new ComunityException("Nu exista un utilizator cu acest username!");
    }

    public void manageConnection(){
        Connection c = connectionService.findConnection(currentUser.getId(),LocalDate.now());
        if(c == null) {
            connectionService.addConnection(currentUser);
            boolean newN = sendNotifications();
            setNewNotification(newN);
        }
    }

    public boolean sendNotifications(){
        Iterable<Event> events = eventService.getUserEvents(currentUser);
        int nr = 0;
        for(Event event:events){
            long duration =Duration.between(LocalDateTime.now(),event.getStartDate()).toDays();
            if( duration < 5 && duration > 1 ) {
                String message = "Evenimentul \""+event.getTitle()+"\" incepe in "+duration+" zile!";
                notificationService.sendNotification(event, currentUser,message);
                nr++;
            }
            else if(duration == 0){
                String message ="Evenimentul \""+event.getTitle()+"\" are loc azi!";
                notificationService.sendNotification(event, currentUser,message);
                nr++;
            }
            else if(duration == 1){
                String message ="Evenimentul \""+event.getTitle()+"\" are loc maine!";
                notificationService.sendNotification(event, currentUser,message);
                nr++;
            }
        }
        return nr!=0;

    }

    public void logOut(){
        currentUser = null;
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public boolean areFriends(User me, User friend){
        Tuple<Long, Long> id1 = new Tuple<>(me.getId(),friend.getId());
        Tuple<Long, Long> id2 = new Tuple<>(friend.getId(),me.getId());
        return friendshipService.findOne(id1) != null ||
                friendshipService.findOne(id2) != null;
    }

    public List<FriendRequestDTO> getAllFriendRequest(User u){
        List<FriendRequestDTO> friendRequestDTOS=new ArrayList<>();
        for(FriendRequest friendRequest:friendRequestService.findAll()){
            if(friendRequest.getId().getRight().equals(u.getId())){
                User sender = userService.findOne(friendRequest.getId().getLeft());
                FriendRequestDTO friendRequestDTO =
                        new FriendRequestDTO(sender.getUsername(),friendRequest.getDate().format(Constants.DATE_TIME_FORMATTER),friendRequest.getStatus().toString());
                friendRequestDTOS.add(friendRequestDTO);
            }
        }
        return friendRequestDTOS;
    }

    public void respondToFriendRequestByUsername(String senderUsername, User receiver, int choice){
        User sender = userService.findByUsername(senderUsername);
        respondToFriendRequest(receiver.getId(),sender.getId(),choice);
        notifyObservers();
    }

    public boolean friendRequestAlreadySent(User from, User to){
        FriendRequest friendRequest = friendRequestService.findOne(new Tuple<Long,Long>(from.getId(),to.getId()));
        return (friendRequest != null) && (friendRequest.getStatus() == Status.PENDING) && (friendRequest.getId().getLeft().equals(from.getId()));
    }

    public boolean isRejected(User from, User to){
        FriendRequest friendRequest = friendRequestService.findOne(new Tuple<Long,Long>(from.getId(),to.getId()));
        return (friendRequest != null) && (friendRequest.getStatus() == Status.REJECTED) && (friendRequest.getId().getLeft().equals(from.getId()));
    }

    public void removeSentFriendRequest(User from, User to){
        FriendRequest friendRequest = friendRequestService.findOne(new Tuple<Long,Long>(from.getId(),to.getId()));
        if(friendRequest!=null && friendRequest.getStatus().equals(Status.PENDING)){
            FriendRequest deletedFr = friendRequestService.deleteFriendRequest(friendRequest.getId());
            if(deletedFr == null)
                throw new ComunityException("Upss..ceva nu a mers bine!");
        }
        else
            throw new ComunityException("Imposibil de retras aceasta cerere.");

    }


    public List<MessageDTO> myInbox(User user){
        return StreamSupport.stream(messageService.getAll().spliterator(),false)
                .filter(x->x.getTo().contains(user))
                .map(x->new MessageDTO(x.getId(),x.getFrom().getFirstName()+" "+x.getFrom().getLastName(),x.getDate().format(Constants.DATE_FORMATTER)))
                .collect(Collectors.toList());
    }

    public int countInboxSize(User user){
        return myInbox(user).size();
    }

    public List<MessageDTO> mySentMessages(User user){
        return StreamSupport.stream(messageService.getAll().spliterator(),false)
                .filter(x->x.getFrom().equals(user))
                .map(x->new MessageDTO(x.getId(),x.getFrom().getFirstName()+" "+x.getFrom().getLastName(),x.getDate().format(Constants.DATE_FORMATTER)))
                .collect(Collectors.toList());
    }

    public int countSentMessages(User user){
        return mySentMessages(user).size();
    }

    public List<Friendship> getFriendshipsFromPeriod(LocalDateTime startDate, LocalDateTime finishDate){
        return StreamSupport.stream(friendshipService.getAll().spliterator(),false)
                .filter(x->(x.getId().getLeft().equals(currentUser.getId()) || x.getId().getRight().equals(currentUser.getId()))
                        &&
                        ((x.getDate().isAfter(startDate) || x.getDate().isEqual(startDate)) &&
                                (x.getDate().isBefore(finishDate) || x.getDate().isEqual(finishDate))))
                .collect(Collectors.toList());
    }

    public List<FriendshipDTO> getFriendshipsFromPeriodDTO(LocalDateTime startDate, LocalDateTime finishDate){
        return getFriendshipsFromPeriod(startDate,finishDate).stream()
                .map(x->{
                    if(x.getId().getLeft().equals(currentUser.getId())) {
                        User friend = userService.findOne(x.getId().getRight());
                        return new FriendshipDTO(friend.getFirstName(), friend.getLastName(),x.getDate());
                    }
                    User friend = userService.findOne(x.getId().getLeft());
                    return new FriendshipDTO(friend.getFirstName(), friend.getLastName(),x.getDate());
                })
                .collect(Collectors.toList());
    }

    public List<Message> getMessagesFromPeriod(User friend, LocalDateTime startDate, LocalDateTime finishDate){
        if(friend == null)
            return StreamSupport.stream(messageService.getAll().spliterator(),false)
                    .filter(x->((x.getDate().isAfter(startDate) || x.getDate().isEqual(startDate)) &&
                            (x.getDate().isBefore(finishDate) || x.getDate().isEqual(finishDate)))
                            && ((x.getFrom().equals(currentUser))|| x.getTo().contains(currentUser)))
                    .collect(Collectors.toList());
        return StreamSupport.stream(messageService.getAll().spliterator(),false)
                .filter(x->((x.getDate().isAfter(startDate) || x.getDate().isEqual(startDate)) &&
                        (x.getDate().isBefore(finishDate) || x.getDate().isEqual(finishDate)))
                        && ((x.getFrom().equals(currentUser) && x.getTo().contains(friend))
                        || (x.getFrom().equals(friend) && x.getTo().contains(currentUser))))
                .collect(Collectors.toList());

    }

    public List<MessageDTO1> getMessagesFromPeriodDTO(User friend, LocalDateTime startDate, LocalDateTime finishDate){
        return getMessagesFromPeriod(friend,startDate,finishDate).stream()
                .map(x-> new MessageDTO1(x.getId(),x.getFrom().getFirstName()+" "+x.getFrom().getLastName(),x.toListToString(),x.getDate().format(Constants.DATE_FORMATTER1),x.getMessage()))
                .collect(Collectors.toList());
    }

    public void validateDates(LocalDateTime startDate, LocalDateTime finishDate){
        if(startDate.isAfter(finishDate))
            throw new ComunityException("Introduceti datele in ordine cronologica!");
    }

    public void createFirstReport(LocalDateTime startDate, LocalDateTime finishDate, String path){
        List<Friendship> friendships = getFriendshipsFromPeriod(startDate,finishDate);
        List<Message> messages = getMessagesFromPeriod(null,startDate,finishDate);

        try {
            com.itextpdf.text.Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(path+"\\raport1.pdf"));
            document.open();
            Paragraph paragraph = new Paragraph("                       Raport 1");
            if(friendships.size()!=0) {
                Paragraph paragraph1 = new Paragraph("In perioada " + startDate.format(Constants.DATE_FORMATTER1) + "  -  " + finishDate.format(Constants.DATE_FORMATTER1) + " v-ati imprietenit cu " + friendships.size() + " persoane noi.");
                document.add(paragraph);
                document.add(paragraph1);
                PdfPTable table = new PdfPTable(2);
                table.addCell("Nume");
                table.addCell("Data");
                for (Friendship friendship : friendships) {
                    if (friendship.getId().getLeft().equals(currentUser.getId())) {
                        User friend = userService.findOne(friendship.getId().getRight());
                        table.addCell(friend.getFirstName() + " " + friend.getLastName());
                    } else {
                        User friend = userService.findOne(friendship.getId().getLeft());
                        table.addCell(friend.getFirstName() + " " + friend.getLastName());
                    }
                    table.addCell(friendship.getDate().format(Constants.DATE_FORMATTER1));
                }
                document.add(new Paragraph(" "));
                document.add(table);
                document.add(new Paragraph(" "));
            }
            else{
                Paragraph paragraph4 = new Paragraph("In perioada "+startDate.format(Constants.DATE_FORMATTER1)+"  -  "+finishDate.format(Constants.DATE_FORMATTER1)+" nu v-ati imprietenit cu nicio persoana noua.");
                document.add(paragraph4);
                document.add(new Paragraph(" "));

            }

            if(messages.size()!=0) {

                PdfPTable table2 = new PdfPTable(4);
                table2.addCell("De la");
                table2.addCell("Pentru");
                table2.addCell("Mesaj");
                table2.addCell("Data");

                for (Message message : messages) {
                    table2.addCell(message.getFrom().getFirstName() + " " + message.getFrom().getLastName());
                    table2.addCell(message.toListToString());
                    table2.addCell(message.getMessage());
                    table2.addCell(message.getDate().format(Constants.DATE_FORMATTER1));
                }
                Paragraph paragraph2 = new Paragraph("In perioada " + startDate.format(Constants.DATE_FORMATTER1) + "  -  " + finishDate.format(Constants.DATE_FORMATTER1) + " ati schimbat " + messages.size() + " mesaj/e noi.\n");
                document.add(new Paragraph(" "));
                document.add(paragraph2);
                document.add(new Paragraph(" "));
                document.add(table2);
            }
            else{
                Paragraph paragraph5 = new Paragraph("In perioada "+startDate.format(Constants.DATE_FORMATTER1)+"  -  "+finishDate.format(Constants.DATE_FORMATTER1)+" nu ati conversat cu nimeni.");
                document.add(paragraph5);
                document.add(new Paragraph(" "));
            }

            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void createSecondReport(User friend, LocalDateTime startDate, LocalDateTime finishDate, String path){
        if(friend == null)
            throw new ComunityException("Alegeti un utilizator din tabelul alaturat.");
        List<Message> messages = getMessagesFromPeriod(friend, startDate,finishDate);
        try {
            com.itextpdf.text.Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(path+"\\raport2.pdf"));
            document.open();
            Paragraph paragraph = new Paragraph("                       Raport 2");

            if(messages.size()!=0) {
                PdfPTable table2 = new PdfPTable(4);
                table2.addCell("De la");
                table2.addCell("Pentru");
                table2.addCell("Mesaj");
                table2.addCell("Data");

                for (Message message : messages) {
                    table2.addCell(message.getFrom().getFirstName() + " " + message.getFrom().getLastName());
                    table2.addCell(message.toListToString());
                    table2.addCell(message.getMessage());
                    table2.addCell(message.getDate().format(Constants.DATE_FORMATTER1));
                }
                Paragraph paragraph2 = new Paragraph("In perioada " + startDate.format(Constants.DATE_FORMATTER1) + "  -  " + finishDate.format(Constants.DATE_FORMATTER1) + " ati schimbat " + messages.size() + " mesaj/e noi.\n");
                document.add(new Paragraph(" "));
                document.add(paragraph2);
                document.add(new Paragraph(" "));
                document.add(table2);
            }
            else{
                Paragraph paragraph5 = new Paragraph("In perioada "+startDate.format(Constants.DATE_FORMATTER1)+"  -  "+finishDate.format(Constants.DATE_FORMATTER1)+" nu ati conversat cu "+ friend.getFirstName()+" "+friend.getLastName()+".");
                document.add(paragraph5);
                document.add(new Paragraph(" "));
            }
            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void createNewEvent(String title, String location, String description, LocalDate startDate, LocalDate finishDate, User user){
        if(startDate == null)
            throw new ComunityException("Introduceti data de inceput!\n");
        if(finishDate == null)
            throw new ComunityException("Introduceti data de sfarsit!\n");
        LocalDateTime date1 = LocalDateTime.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth(), 0, 1);
        LocalDateTime date2 = LocalDateTime.of(finishDate.getYear(), finishDate.getMonth(), finishDate.getDayOfMonth(), 0, 1);
        eventService.createNewEvent(title,description,location,date1,date2,user);
    }

    public void addParticipantToEvent(Long idEvent, Long idUser){
        Thread t = new Thread(()-> {
            eventService.addParticipantToEvent(idEvent, idUser);
            Event event = eventService.findOne(idEvent);
            String message = "V-ati incris la evenimentul \"" + event.getTitle() + "\".";
            synchronized (notificationService) {
                notificationService.sendNotification(event, userService.findOne(idUser), message);
            }
        });
        t.start();
        setNewNotification(true);
    }

    public void removeParticipantFromEvent(Long idEvent, Long idUser){
        Thread t = new Thread(()-> {
            eventService.removeParticipantFromEvent(idEvent, idUser);
            Event event = eventService.findOne(idEvent);
            String message = "Nu mai participati la evenimentul \"" + event.getTitle() + "\".";
            synchronized (notificationService) {
                notificationService.sendNotification(event, userService.findOne(idUser), message);
            }
        });
        t.start();
        notifyObservers();
        setNewNotification(true);
    }

    public void updateUserChoice(Event event, User participant, Boolean notificationChoice){
        eventService.updateUserChoice(event.getId(),participant.getId(),notificationChoice);
        String message = null;
        if(notificationChoice) {
            message = "Ati optat pentru primirea notificarilor legate de evenimentul \"" + event.getTitle() + "\".";
        }
        else{
            message = "Nu veti mai primi notificari legate de evenimentul \"" + event.getTitle() + "\".";
        }
        synchronized (notificationService) {
            notificationService.sendNotification(event, participant, message);
        }
        setNewNotification(true);
    }


    public List<FriendRequestDTO> getFriendRequestsPaging(int index, int limit, User user){
       return friendRequestService.getAllPaging(index, limit, user)
                .map(x->{
                    User from = userService.findOne(x.getId().getLeft());
                    return new FriendRequestDTO(from.getUsername(),x.getDate().format(Constants.DATE_FORMATTER),x.getStatus().toString());
                })
                .collect(Collectors.toList());
    }


    List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(Observer::update);
    }
}
