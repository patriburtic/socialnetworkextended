package socialnetwork.service;

import socialnetwork.domain.Connection;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.repository.database.ConnectionsRepositoryDB;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ConnectionService {

    private final ConnectionsRepositoryDB repo;

    public ConnectionService(ConnectionsRepositoryDB repo) {
        this.repo = repo;
    }

    public Connection findConnection(Long id, LocalDate date){
        return repo.findOne(id, date);
    }

    public Connection addConnection(User user){
        Connection connection = new Connection(user);
        connection.setId(findNextID());
        return repo.save(connection);
    }

    private long findNextID(){
        long max = 0;
        for(Connection u: repo.findAll()){
            if(u.getId() > max)
                max = u.getId();
        }
        return max+1;
    }


}
