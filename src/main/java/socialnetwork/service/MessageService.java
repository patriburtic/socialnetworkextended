package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.User;
import socialnetwork.repository.database.MessageRepositoryDB;
import socialnetwork.repository.file.MessageFile;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import utils.Constants;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MessageService {
    private MessageRepositoryDB repo;

    public MessageService(MessageRepositoryDB repo) {
        this.repo = repo;
    }

    /**
     *
     * @param m - message to be saved
     * @return null if successful, m otherwise
     * This method automatically sets the id the messages and saves the given entity
     */
    public Message addMessage(Message m){
        m.setId(findNextID());
        return repo.save(m);
    }

    /**
     *
     * @param id - id of the message we want to delete
     * @return the message with the given id if successful, null otherwise
     */
    public Message deleteMessage (Long id){
        return repo.delete(id);
    }

    /**
     *
     * @param id - the id of the entity we want to find
     * @return the entity with the given id, if found, null otherwise
     */
    public Message findOne(Long id){
        return repo.findOne(id);
    }

    /**
     *
     * @return all messages
     */
    public Iterable<Message> getAll(){
        return repo.findAll();
    }

    public Iterable<Message> findAllPaging(int pageNumber, int pageSize){
        Pageable pageable = new PageableImplementation(pageNumber, pageSize);
        Page<Message> currentPage = repo.findAll(pageable);
        Stream<Message> messageStream = currentPage.getContent();
        return messageStream.collect(Collectors.toList());
    }

    /**
     *
     * @return the next available id
     */
    private long findNextID(){
        long max = 0;
        for(Message u: getAll()){
            if(u.getId() > max)
                max = u.getId();
        }
        return max+1;
    }

    /**
     *
     * @param message - message we want to reply to
     * @param reply - the reply
     * @return the reply if successful, null otherwise
     */
    public Message reply(Message message, Message reply){
        reply.setId(findNextID());
        return repo.replyToMessage(message,reply);
    }

    /**
     *
     * @param u1 - user
     * @param u2 -user
     * @return a list of messages containing the conversation between u1 and u2, sorted by date
     */
    public List<Message> showConversation(User u1, User u2){
        List<Message> conversations = new ArrayList<>();
        for (Message message : repo.findAll()) {
            if((message.getFrom().getId() == u1.getId() && message.getTo().stream().filter(x-> x.getId()==u2.getId()).count() != 0) ||
                    (message.getFrom().getId()== u2.getId() && message.getTo().stream().filter(x-> x.getId()==u1.getId()).count() != 0)){
                conversations.add(message);
            }
        }
        conversations.sort(new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        return conversations;
    }

    public List<MessageDTO> inbox(int index, int limit, User user){
        Pageable pageable = new PageableImplementation(index,limit);
        Page<Message> page = repo.userPagingReceivedMessages(pageable, user.getId());
        return page.getContent()
                .map(x->new MessageDTO(x.getId(),x.getFrom().getFirstName()+" "+x.getFrom().getLastName(),x.getDate().format(Constants.DATE_FORMATTER)))
                .collect(Collectors.toList());
    }

    public int countInboxSize(User user){
        Pageable pageable = new PageableImplementation(1,100);
        Page<Message> page = repo.userPagingReceivedMessages(pageable, user.getId());
        int size = (int) page.getContent().count();
        page = repo.userPagingReceivedMessages(page.nextPageable(), user.getId());
        while(page.getContent().count() != 0){
            page = repo.userPagingReceivedMessages(page.nextPageable(), user.getId());
            size += (int) page.getContent().count();
        }
        return size;
    }


    public int countSentMessages(User user){
        Pageable pageable = new PageableImplementation(1,100);
        Page<Message> page = repo.userPagingSentMessages(pageable, user.getId());
        int size = (int) page.getContent().count();
        page = repo.userPagingSentMessages(page.nextPageable(), user.getId());
        while(page.getContent().count() != 0){
            page = repo.userPagingSentMessages(page.nextPageable(), user.getId());
            size += (int) page.getContent().count();
        }
        return size;
    }


    public List<MessageDTO> sentMessages(int index, int limit, User user){
        Pageable pageable = new PageableImplementation(index,limit);
        Page<Message> page = repo.userPagingSentMessages(pageable, user.getId());
        return page.getContent()
                .map(x->new MessageDTO(x.getId(),x.getFrom().getFirstName()+" "+x.getFrom().getLastName(),x.getDate().format(Constants.DATE_FORMATTER)))
                .collect(Collectors.toList());
    }


}
