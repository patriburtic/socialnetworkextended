package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.Message;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.repository.database.NotificationsRepositoryDB;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class NotificationService {
    private NotificationsRepositoryDB repo;

    public NotificationService(NotificationsRepositoryDB repo) {
        this.repo = repo;
    }

    public Notification sendNotification(Event event, User u, String message){
        Notification notification = new Notification(event,u, message, LocalDateTime.now());
        notification.setId(findNextID());
        return repo.save(notification);
    }

    public Notification deleteNotification(Long id){
        return repo.delete(id);
    }

    public Set<Notification> getAllUserNotifications(Long id){
        return repo.findAllNotifications(id);
    }

    public Page<Notification> getAllUserNotificationsPaged(Pageable pageable, Long id){
        return repo.findAll(pageable, id);
    }

    private long findNextID(){
        long max = 0;
        for(Notification u: repo.findAll()){
            if(u.getId() > max)
                max = u.getId();
        }
        return max+1;
    }
}
