package socialnetwork.domain.utils;

/**
 * this enum determines the status of a friend request
 * this enum will be converted in Integer as:
 * PENDING -> 0
 * APPROVED -> 1
 * REJECTED -> 2
 * UNKNOWN -> any Integer except 0, 1 and 2
 */
public enum Status {
    PENDING,
    APPROVED,
    REJECTED,
    UNKNOWN
}
