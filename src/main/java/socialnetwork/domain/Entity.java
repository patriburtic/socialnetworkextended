package socialnetwork.domain;

import java.io.Serializable;

/**
 * Define an object that has an ID
 * @param <ID>
 */

public class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 7331115341259248461L;
    private ID id;

    /**
     *
     * @return ID
     */
    public ID getId() {
        return id;
    }

    /**
     * Set the ID
     * @param id
     */
    public void setId(ID id) {
        this.id = id;
    }
}