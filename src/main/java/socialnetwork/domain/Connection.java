package socialnetwork.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Connection extends Entity<Long> {
    private final User user;
    private LocalDate date;

    public Connection(User user) {
        this.user = user;
        date = LocalDate.now();
    }

    public User getUser() {
        return user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
