package socialnetwork.domain;

public class FriendDTO extends UserDTO{
    private String date;

    public FriendDTO(String firstName, String lastName, String username, String date) {
        super(firstName, lastName, username);
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
