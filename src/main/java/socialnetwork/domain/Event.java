package socialnetwork.domain;

import utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Event extends Entity<Long> {
    private final String title;
    private final String description;
    private final String location;
    private List<Tuple<User,Boolean>> participants;
    private User creator;
    private final LocalDateTime startDate;
    private final LocalDateTime finishDate;

    public Event(String title, String description, String location, LocalDateTime startDate, LocalDateTime finishDate) {
        this.title = title;
        this.description = description;
        this.location = location;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.participants = new ArrayList<>();
        this.creator = null;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public List<Tuple<User,Boolean>> getParticipants() {
        return participants;
    }

    public List<User> getParticipantsOnly(){
        return participants.stream()
                .map(Tuple::getLeft)
                .collect(Collectors.toList());
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getFinishDate() {
        return finishDate;
    }

    public void addParticipant(User participant, Boolean notification){
        this.participants.add(new Tuple<>(participant,notification));
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getPeriod(){
        return startDate.format(Constants.DATE_FORMATTER1)+" -> "+finishDate.format(Constants.DATE_FORMATTER1);
    }
    @Override
    public String toString() {
        return "Event{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", participants=" + participants +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                '}';
    }
}
