package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public class Friendship extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;
    public Friendship() {
        date = LocalDateTime.now();
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @param date
     * set date
     */
    public void setDate(LocalDateTime date){this.date = date;}
}
