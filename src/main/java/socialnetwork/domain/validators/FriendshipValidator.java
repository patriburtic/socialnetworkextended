package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;

/**
 * FriendshipValidator implements the Validator interface
 */

public class FriendshipValidator implements Validator<Friendship> {
    /**
     *
     * @param entity (Friendship)
     * @throws ValidationException if the given entity is invalid
     * this method checks if the given has a not null ID and date
     */
    @Override
    public void validate(Friendship entity) throws ValidationException {
        String message =  "";
        if(entity.getId() == null)
            message += "ID invalid!\n";
        if(entity.getId() != null) {
            if (entity.getId().getLeft() < 0)
                message += "Primul ID invalid!\n";
            if (entity.getId().getRight() < 0)
                message += "Al doilea ID invalid!\n";
        }
        if(entity.getDate() == null)
            message += "Data invalida!\n";
        if(message.length() > 0)
            throw new ValidationException(message);
    }
}
