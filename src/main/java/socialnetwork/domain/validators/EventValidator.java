package socialnetwork.domain.validators;

import socialnetwork.domain.Event;

public class EventValidator implements Validator<Event> {
    @Override
    public void validate(Event entity) throws ValidationException {
        String errors = "";
        if(entity.getTitle().equals(""))
            errors += "Completati titlul evenimentului!\n";
        if(entity.getTitle().length() > 50)
            errors += "Va rugam sa dati un titlu mai scurt acestui eveniment (max 50 caractere).\n";
        if(entity.getDescription().equals(""))
            errors += "Completati descrierea evenimentului!\n";
        if(entity.getDescription().length() > 300)
            errors += "Va rugam sa scrieti o descriere mai scurta pentru acest eveniment (max 300 caractere).\n";
        if(entity.getLocation().equals(""))
            errors += "Completati locatia evenimentului!\n";
        if(entity.getStartDate() == null)
            errors += "Completati data de inceput a evenimentului!\n";
        if(entity.getFinishDate() == null)
            errors += "Completati data de final a evenimentului!\n";
        if(entity.getStartDate().isAfter(entity.getFinishDate()))
            errors += "Completati datele de inceput si final in ordine cronologica.\n";
        if(errors.length()>0)
            throw new ValidationException(errors);

    }
}
