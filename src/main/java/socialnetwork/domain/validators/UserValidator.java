package socialnetwork.domain.validators;

import socialnetwork.domain.User;

/**
 * UserValidator implements the Validator interface
 */

public class UserValidator implements Validator<User> {
    /**
     *
     * @param entity (User)
     * @throws ValidationException if the given entity is invalid
     * this method checks if the given has a valid firstName, lastName, and a not null ID
     */
    @Override
    public void validate(User entity) throws ValidationException {
        String message = "";
        if(!entity.getFirstName().matches("[A-Z][a-z]*([ -]?[A-Z][a-z]*){0,3}"))
            message += "Prenume invalid!\n";
        if(entity.getFirstName().length() > 50)
            message += "Prenume prea lung!\n";
        if(!entity.getLastName().matches("[A-Z][a-z]*([ -]?[A-Z][a-z]*){0,3}"))
            message += "Nume invalid!\n";
        if(entity.getLastName().length() > 50)
            message += "Nume prea lung!\n";
        if(entity.getFirstName().equals(""))
            message += "Introduceti prenumele dumneavoastra.\n";
        if(entity.getLastName().equals(""))
            message += "Introduceti numele dumneavoastra.\n";
        if(entity.getId() == null)
            message += "ID nul!\n";
        if(entity.getUsername().equals(""))
            message += "Numele de utilizator lipseste!\n";
        if(entity.getUsername().length() > 30)
            message+= "Nume de utilizator prea lung!\n";
        if(entity.getPassword().length() <=8)
            message+= "Parola prea scurta! Introduceti o parola de minim 9 caractere\n";
//        if(!entity.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"))
//            message += "Parola trebuie sa contina cel putin o litera si o cifra in compozitie.\n";


        if(message.length() != 0)
            throw new ValidationException(message);
    }

}