package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

/**
 * MessageValidator implements the Validator interface
 */

public class MessageValidator implements Validator<Message>{
    /**
     *
     * @param entity (Message)
     * @throws ValidationException if the given entity is invalid
     * this method checks if the given has a valid id, from, to and date
     */
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors = "";
        if(entity.getId() == null || entity.getId() < 0)
            errors += "ID invalid!\n";
        if(entity.getFrom() == null)
            errors += "Expeditor invalid!\n";
        if(entity.getTo().size() == 0)
            errors += "Mesajul trebuie sa aiba cel putin un destinatar!\n";
        if(entity.getDate() == null)
            errors += "Data invalida!\n";
        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
