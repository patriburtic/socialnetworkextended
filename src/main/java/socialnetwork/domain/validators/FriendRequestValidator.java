package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;

/**
 * FriendRequest validator implements the Validator interface
 */

public class FriendRequestValidator implements Validator<FriendRequest>{
    /**
     *
     * @param entity (FriendRequest)
     * @throws ValidationException if the given entity is invalid
     *this method checks if the given has a not null ID and a not null date
     */
    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        String errors = "";
        if(entity.getId() == null)
            errors += "ID invalid!\n";
        if(entity.getDate() == null)
            errors += "Data invalida!\n";
        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
