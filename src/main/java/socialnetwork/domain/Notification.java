package socialnetwork.domain;

import utils.Constants;

import java.time.LocalDateTime;

public class Notification extends Entity<Long>{
    private Event event;
    private User user;
    private String message;
    private LocalDateTime date;
    private String dateString;

    public Notification(Event event, User user, String message, LocalDateTime date) {
        this.event = event;
        this.user = user;
        this.message = message;
        this.date = date;
        this.dateString = date.format(Constants.DATE_FORMATTER);
    }

    public Event getEvent() {
        return event;
    }

    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDateString() {
        return dateString;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "event=" + event +
                ", user=" + user +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }
}
