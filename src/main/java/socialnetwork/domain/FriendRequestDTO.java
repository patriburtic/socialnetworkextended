package socialnetwork.domain;

public class FriendRequestDTO {
    private String username;
    private String date;
    private String status;

    public FriendRequestDTO(String username, String date, String status) {
        this.username = username;
        this.date = date;
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }
}
