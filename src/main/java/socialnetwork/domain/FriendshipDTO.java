package socialnetwork.domain;

import utils.Constants;

import java.time.LocalDateTime;

public class FriendshipDTO {
    private String completeName;
    private String date;

    public FriendshipDTO(String firstName, String lastName, LocalDateTime date) {
        this.completeName = firstName+" "+lastName;
        this.date = date.format(Constants.DATE_FORMATTER1);
    }

    public String getCompleteName() {
        return completeName;
    }

    public String getDate() {
        return date;
    }
}
