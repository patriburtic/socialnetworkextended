package socialnetwork.domain;

import socialnetwork.domain.MessageDTO;

public class MessageDTO1 extends MessageDTO {
    private final String message;
    private final String to;

    public MessageDTO1(Long id, String from, String to, String date, String message) {
        super(id, from, date);
        this.to = to;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getTo() {
        return to;
    }
}
