package socialnetwork.domain;

import utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Define a message
 * extends Entity which means this class will have an ID
 */
public class Message extends Entity<Long>{
    private User from;
    private List<User> to;
    private LocalDateTime date;
    private String message;
    private Message replyTo;

    /**
     *
     * @param from - the sender
     * @param to - the receiver
     * @param message - the sent message
     * This constructor sets the date to the current local date time and the replyTo to null by default
     */
    public Message(User from, List<User> to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
        date = LocalDateTime.now();
        replyTo = null;
    }

    /**
     *
     * @param message - Message
     * This constructor is used when the reply is sent
     *                sets the date to current local date time
     *                      replyTo, from and to to null by default
     */
    public Message(String message) {
        this.message = message;
        date=LocalDateTime.now();
        replyTo = null;
        from = null;
        to = null;
    }

    /**
     *
     * @return the sender
     */
    public User getFrom() {
        return from;
    }

    /**
     * sets the sender
     * @param from - the update sender
     */
    public void setFrom(User from) {
        this.from = from;
    }

    /**
     *
     * @return the receivers (List</User>)
     */
    public List<User> getTo() {
        return to;
    }

    /**
     * sets the updated list of receivers
     * @param to - list of receivers (List</User>)
     */

    public void setTo(List<User> to) {
        this.to = to;
    }

    /**
     *
     * @return the sent message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return replyTo - the message to which this Message replies to
     */
    public Message getReplyTo() {
        return replyTo;
    }

    /**
     * sets the message to which this Message replies to
     * @param replyTo - Message
     */
    public void setReplyTo(Message replyTo) {
        this.replyTo = replyTo;
    }

    /**
     * @return date - the date when this message was sent
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * sets the date
     * @param date - LocaldateTime
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * converts the list of receivers to String
     * @return string - String
     */
    public String toListToString(){
        String string = "";
        int i = 0;
        if(to.size() != 0) {
            if (to.size() > 2) {
                for (i = 0; i < to.size()-2; i++) {
                    string += to.get(i).getFirstName() + " " + to.get(i).getLastName() + ", ";
                }
                string += to.get(i).getFirstName() + " " + to.get(i).getLastName() + " si ";
                i++;
                string += to.get(i).getFirstName() + " " + to.get(i).getLastName();
            } else if (to.size() == 2) {
                string += to.get(i).getFirstName() + " " + to.get(i).getLastName() + " si ";
                i++;
                string += to.get(i).getFirstName() + " " + to.get(i).getLastName();
            } else {
                string += to.get(i).getFirstName() + " " + to.get(i).getLastName();
            }
        }
        return string;
    }

    /**
     *
     * @return a string form of this object
     */
    @Override
    public String toString() {
        return getId()+"."+" On " + date.format(Constants.DATE_TIME_FORMATTER) +
                " " + from.getFirstName() + " " + from.getLastName() +
                " wrote to " + toListToString() +
                ":'" + message + '\'';//+" \n\t Replies to: \n\t"+ replyTo;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return getFrom().equals(message1.getFrom()) &&
                getTo().equals(message1.getTo()) &&
                date.equals(message1.date) &&
                getMessage().equals(message1.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), date, getMessage());
    }
}
