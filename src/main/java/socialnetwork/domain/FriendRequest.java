package socialnetwork.domain;

import socialnetwork.domain.utils.Status;
import utils.Constants;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * FriendRequest is a class that shapes a friend request
 * extends Entity which means that it has an ID (Tuple<Long,Long>)
 */

public class FriendRequest extends Entity<Tuple<Long,Long>> {
    private Status status;
    private LocalDateTime date;

    /**
     * Constructor
     * sets the status by default to PENDING and the date to the current local date time
     */
    public FriendRequest() {
        status=Status.PENDING;
        date=LocalDateTime.now();
    }

    /**
     *
     * @return the current status (enum Status)
     */
    public Status getStatus() {
        return status;
    }

    /**
     *
     * @param status (the new status)
     * if the status has changed, this method changes the current status in the object to the new one
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     *
     * @return the date when this friend request was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @param date (LocalDateTime)
     * this method is used when we import data from databases or files and we need to set the date
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "status=" + status +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FriendRequest)) return false;
        FriendRequest that = (FriendRequest) o;
        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
