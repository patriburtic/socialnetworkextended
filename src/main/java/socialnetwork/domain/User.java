package socialnetwork.domain;

import org.mindrot.jbcrypt.BCrypt;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * defines a user
 * extends Entity which means this object has an ID
 */
public class User extends Entity<Long>{
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private List<User> friends;

    /**
     *
     * @param firstName - user's first name
     * @param lastName - user's last name
     */
    public User(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = BCrypt.hashpw(password,BCrypt.gensalt());

        friends = new ArrayList<>();
    }

    /**
     *
     * @return firstName - user's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * sets user's first name
     * @param firstName - String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return lastName - user's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * sets user's last name
     * @param lastName - String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return friends - user's list of friends
     */
    public List<User> getFriends() {
        return friends;
    }

    /**
     * sets user's list of friends
     * @param friends - List</User>
     */
    public void setFriends(List<User> friends){ this.friends = friends; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password){
        this.password = password;
    }

    @Override
    public String toString() {
        return  firstName+" "+lastName+" ("+username+") " ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }
}