package socialnetwork.domain;


public class MessageDTO {
    private final Long id;
    private final String from;
    private final String date;

    public MessageDTO(Long id, String from, String date) {
        this.id = id;
        this.from = from;
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public String getDate() {
        return date;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return getFrom()+" "+getDate();
    }
}
