package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controllers.LoginController;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.database.*;
import socialnetwork.service.*;

public class MainFX extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loaderLogin = new FXMLLoader();
        loaderLogin.setLocation(getClass().getResource("/view/loginViewNew.fxml"));
        BorderPane loginRoot = loaderLogin.load();
        LoginController loginController = loaderLogin.getController();



        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
        final String password= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");



        FriendshipRepositoryDB friendshipRepository = new FriendshipRepositoryDB(url,username,password,new FriendshipValidator());
        UserRepositoryDB userRepository = new UserRepositoryDB(url,username,password,new UserValidator());
        MessageRepositoryDB messageRepository = new MessageRepositoryDB(url,username,password,new MessageValidator());
        FriendRequestRepositoryDB friendRequestRepository = new FriendRequestRepositoryDB(url,username,password,new FriendRequestValidator());
        EventRepositoryDB eventRepositoryDB = new EventRepositoryDB(url,username,password,new EventValidator());
        NotificationsRepositoryDB notificationsRepositoryDB = new NotificationsRepositoryDB(url,username,password,eventRepositoryDB,userRepository);
        ConnectionsRepositoryDB connectionsRepositoryDB = new ConnectionsRepositoryDB(url,username,password,userRepository);


        UserService userService = new UserService(userRepository);
        FriendshipService friendshipService = new FriendshipService(friendshipRepository);
        MessageService messageService = new MessageService(messageRepository);
        FriendRequestService friendRequestService = new FriendRequestService(friendRequestRepository);
        EventService eventService = new EventService(eventRepositoryDB);
        NotificationService notificationService = new NotificationService(notificationsRepositoryDB);
        ConnectionService connectionService = new ConnectionService(connectionsRepositoryDB);
        PageService pageService = new PageService(friendshipService, userService, messageService, friendRequestService,eventService,notificationService,connectionService);


        primaryStage.setScene(new Scene(loginRoot, 600, 400));
        primaryStage.setTitle("Bun venit!");
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setMaxWidth(600);
        loginController.setContext(pageService, primaryStage);
        primaryStage.getIcons().add(new Image("/images/Hypen.png"));
        primaryStage.show();


    }

    public static void main(String[] args) {
        launch(args);
    }

}
