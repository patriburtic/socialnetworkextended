package socialnetwork.repository.file;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.utils.Status;
import socialnetwork.domain.validators.Validator;
import utils.Constants;

import java.time.LocalDateTime;
import java.util.List;

public class FriendRequestFile extends AbstractFileRepository<Tuple<Long,Long>, FriendRequest>{

    public FriendRequestFile(String fileName, Validator<FriendRequest> validator) {
        super(fileName, validator);
    }

    @Override
    public FriendRequest extractEntity(List<String> attributes) {
        long from = Long.parseLong(attributes.get(0));
        long to = Long.parseLong(attributes.get(1));
        Tuple<Long,Long> id = new Tuple<>(from,to);
        LocalDateTime date = LocalDateTime.parse(attributes.get(2));
        int statusID = Integer.parseInt(attributes.get(3));
        Status status;
        switch (statusID){
            case 0:
                status = Status.PENDING;
                break;
            case 1:
                status = Status.APPROVED;
                break;
            case 2:
                status = Status.REJECTED;
                break;
            default:
                status = Status.UNKNOWN;
                break;
        }
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setId(id);
        friendRequest.setDate(date);
        friendRequest.setStatus(status);
        return friendRequest;
    }

    @Override
    protected String createEntityAsString(FriendRequest entity) {
        int statusId = 0;
        if(entity.getStatus()==Status.PENDING)
            statusId=0;
        else if(entity.getStatus()==Status.APPROVED)
            statusId=1;
        else if(entity.getStatus()==Status.REJECTED)
            statusId=2;
        else
            statusId=3;
        return entity.getId().getLeft()+";"
                +entity.getId().getRight()+";"
                +entity.getDate()+";"
                +statusId;
    }


}