package socialnetwork.repository.file;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

public class FriendshipFile extends AbstractFileRepository<Tuple<Long,Long>, Friendship> {
    public FriendshipFile(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    /**
     *
     * @param attributes not null
     * @return Friendship based on a list of attributes
     */
    @Override
    public Friendship extractEntity(List<String> attributes) {
        Tuple<Long,Long> tuple = new Tuple<>(Long.parseLong(attributes.get(0)),Long.parseLong(attributes.get(1)));
        Friendship friendship = new Friendship();
        friendship.setId(tuple);
        LocalDateTime date = LocalDateTime.parse(attributes.get(2));
        friendship.setDate(date);

        return friendship;
    }

    /**
     *
     * @param entity not null
     * @return a String based on an entity
     */

    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight()+";"+entity.getDate();
    }
}
