package socialnetwork.repository.file;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageFile extends AbstractFileRepository<Long, Message> {
    private UserFile userFile;
    public MessageFile(String fileName, Validator<Message> validator, UserFile userFile) {
        super(validator, fileName);
        this.userFile = userFile;
        super.loadData();
    }

    @Override
    public Message extractEntity(List<String> attributes) {
        int i = 0;
        long id = Long.parseLong(attributes.get(i++));
        long fromId = Long.parseLong(attributes.get(i++));
        User from = userFile.findOne(fromId);
        int nrTo = Integer.parseInt(attributes.get(i++));
        List<User> toList = new ArrayList<>();
        for(int j = i; j < nrTo+i; j++){
            long idTo = Long.parseLong(attributes.get(j));
            toList.add(userFile.findOne(idTo));
        }
        i += nrTo;
        String text = attributes.get(i++);
        long replyId = Long.parseLong(attributes.get(i++));
        LocalDateTime date = LocalDateTime.parse(attributes.get(i));

        Message message = new Message(from, toList, text);
        message.setDate(date);
        message.setId(id);
        if(replyId == 0)
            message.setReplyTo(null);
        else
            message.setReplyTo(findOne(replyId));

        return message;
    }

    @Override
    protected String createEntityAsString(Message entity) {
        String s = entity.getId()+";"
                +entity.getFrom().getId()+";"
                +entity.getTo().size()+";";
        for(User u:entity.getTo()){
            s += u.getId() + ";";
        }
        s += entity.getMessage()+";";
        if(entity.getReplyTo() != null)
            s += entity.getReplyTo().getId()+";";
        else
            s += "0;";
        s += entity.getDate();
        return s;
    }

    public void setUtilizatorFile(UserFile userFile) {
        this.userFile = userFile;
    }

    public Message replyToMessage(Message message, Message reply){
        reply.setReplyTo(message);
        Message saved = save(reply);
        return saved;
    }


}
