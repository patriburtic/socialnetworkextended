package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageRepositoryDB implements PagingRepository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;
    private Connection connection;
    /**
     *
     * @param url - the link to database
     * @param username - required to connect to the database
     * @param password - required to connect to the database
     * @param validator - validates the data
     */
    public MessageRepositoryDB(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     *
     * @param aLong -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     *          or null - if there is no entity with the given id
     * @throws IllegalArgumentException
     *                  if id is null.
     */
    @Override
    public Message findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        String SQL_message = "select * from domain.messages where id = ?";
        String SQL_messages_users_link =
                "select distinct id_user, first_name, last_name, username, password " +
                        "from domain.messages INNER JOIN domain.messages_users_link mul " +
                        "on ? = mul.id_message INNER JOIN domain.users u " +
                        "on u.id = mul.id_user";
        String SQL_get_some_user = "select first_name, last_name, username, password from domain.users where id = ?";
        Message message = null;
        try(

                PreparedStatement statement1 = connection.prepareStatement(SQL_message);
                PreparedStatement statement2 = connection.prepareStatement(SQL_messages_users_link);
                PreparedStatement statement3 = connection.prepareStatement(SQL_get_some_user)
                ) {
            statement1.setLong(1,aLong);
            ResultSet resultSet1 = statement1.executeQuery();
            while(resultSet1.next()) {
                Long fromId = resultSet1.getLong("from");
                String txt = resultSet1.getString("text");
                LocalDateTime date = LocalDateTime.parse(resultSet1.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                Long replyToId = resultSet1.getLong("reply_to_id");
                statement2.setLong(1, aLong);
                ResultSet resultSet2 = statement2.executeQuery();
                List<User> to = new ArrayList<>();
                while (resultSet2.next()) {
                    Long toId = resultSet2.getLong("id_user");
                    String toFirstName = resultSet2.getString("first_name");
                    String toLastName = resultSet2.getString("last_name");
                    String toUsername = resultSet2.getString("username");
                    String toPassword = resultSet2.getString("password");
                    User toUser = new User(toFirstName, toLastName, toUsername, toPassword);
                    toUser.setId(toId);
                    to.add(toUser);
                }
                statement3.setLong(1, fromId);
                ResultSet resultSet3 = statement3.executeQuery();
                User from = null;
                while(resultSet3.next()) {
                    String fromFirstName = resultSet3.getString("first_name");
                    String fromLastName = resultSet3.getString("last_name");
                    String fromUsername= resultSet3.getString("username");
                    String fromPassword = resultSet3.getString("password");
                    from = new User(fromFirstName, fromLastName, fromUsername, fromPassword);
                }
                from.setId(fromId);
                message = new Message(from, to, txt);
                message.setDate(date);
                message.setId(aLong);
                if (replyToId == 0)
                    return message;
                else {
                    Message replyToMessage = findOne(replyToId);
                    message.setReplyTo(replyToMessage);
                    return message;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }
    /**
     *
     * @return all messages
     */
    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        String SQL_messages = "select * from domain.messages";
        String SQL_messages_users_link =
                "select distinct id_user, first_name, last_name, username, password " +
                        "from domain.messages INNER JOIN domain.messages_users_link mul " +
                        "on ? = mul.id_message INNER JOIN domain.users u " +
                        "on u.id = mul.id_user";
        String SQL_get_some_user = "select first_name, last_name, username, password from domain.users where id = ?";
        try(
            PreparedStatement statement1 = connection.prepareStatement(SQL_messages);
            ResultSet resultSet1 = statement1.executeQuery();
            PreparedStatement statement2 = connection.prepareStatement(SQL_messages_users_link);
            PreparedStatement statement3 = connection.prepareStatement(SQL_get_some_user)){
            while(resultSet1.next()){
                Long id = resultSet1.getLong("id");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet1.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                Long fromID = resultSet1.getLong("from");
                statement3.setLong(1,fromID);
                ResultSet resultSet3 = statement3.executeQuery();
                User from = null;
                while(resultSet3.next()) {
                    String firstName = resultSet3.getString("first_name");
                    String last_name = resultSet3.getString("last_name");
                    String username1 = resultSet3.getString("username");
                    String password = resultSet3.getString("password");
                    from = new User(firstName, last_name, username1, password);
                }
                from.setId(fromID);
                String txt = resultSet1.getString("text");
                Long replyToId = resultSet1.getLong("reply_to_id");
                Message replyToMessage = null;
                if(replyToId != 0)
                     replyToMessage = findOne(replyToId);
                List<User> to = new ArrayList<>();
                statement2.setLong(1,id);
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next()){
                    String toFirstName = resultSet2.getString("first_name");
                    String toLastName = resultSet2.getString("last_name");
                    String toUsername = resultSet2.getString("username");
                    String toPassword = resultSet2.getString("password");
                    Long toId = resultSet2.getLong("id_user");
                    User toUser = new User(toFirstName,toLastName, toUsername, toPassword);
                    toUser.setId(toId);
                    to.add(toUser);
                }
                Message message = new Message(from, to, txt);
                message.setId(id);
                message.setDate(dateTime);
                message.setReplyTo(replyToMessage);
                messages.add(message);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }
    /**
     *
     * @param entity
     *         entity must be not null
     * @return null- if the given entity is saved
     *         otherwise returns the entity (id already exists)
     * @throws ValidationException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     */
    @Override
    public Message save(Message entity) {
        if (entity==null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);
        String SQL_message = "insert into domain.messages values (?, ?, ?, ?, ?)";
        String SQL_messages_users_link = "insert into domain.messages_users_link values (?, ?)";
        try(
            PreparedStatement statement1 = connection.prepareStatement(SQL_message);
            PreparedStatement statement2 = connection.prepareStatement(SQL_messages_users_link)){
            statement1.setLong(1,entity.getId());
            statement1.setTimestamp(2, Timestamp.valueOf(entity.getDate().format(Constants.DATE_TIME_FORMATTER)));
            statement1.setLong(3,entity.getFrom().getId());
            statement1.setString(4,entity.getMessage());
            if(entity.getReplyTo() == null || entity.getReplyTo().getId() == null)
                statement1.setNull(5,0);
            else
                statement1.setLong(5,entity.getReplyTo().getId());
            statement1.executeUpdate();
            for(User u: entity.getTo()){
                statement2.setLong(1,entity.getId());
                statement2.setLong(2,u.getId());
                statement2.executeUpdate();
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;

    }
    /**
     *  removes the entity with the specified id
     * @param aLong
     *      id must be not null
     * @return the removed entity or null if there is no entity with the given id
     * @throws IllegalArgumentException
     *                   if the given id is null.
     */
    @Override
    public Message delete(Long aLong) {
        if (aLong == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL_delete_message = "Delete from domain.messages where id = ?";
        String SQL_delete_receivers = "Delete from domain.messages_users_link where id_message = ?";
        String SQL_update = "Update domain.messages set reply_to_id = null where reply_to_id = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL_delete_message);
            PreparedStatement statement1 = connection.prepareStatement(SQL_delete_receivers);
            PreparedStatement statement2 = connection.prepareStatement(SQL_update)){
            Message message = findOne(aLong);
            statement1.setLong(1,aLong);
            statement1.executeUpdate();
            statement2.setLong(1,aLong);
            statement2.executeUpdate();
            statement.setLong(1,aLong);
            statement.executeUpdate();

            return message;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    /**
     *
     * @param message - message to reply to
     * @param reply - the reply
     * @return the reply if successful, null otherwise
     */
    public Message replyToMessage(Message message, Message reply) {
        reply.setReplyTo(message);
        return save(reply);
    }

    @Override
    public Page<Message> findAll(Pageable pageable) {
//        Paginator<Message> paginator = new Paginator<>(pageable,findAll());
//        return paginator.paginate();
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Message> messages = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select * from domain.messages order by id limit ? offset ?";
        String SQL_messages_users_link =
                "select distinct id_user, first_name, last_name, username, password " +
                        "from domain.messages INNER JOIN domain.messages_users_link mul " +
                        "on ? = mul.id_message INNER JOIN domain.users u " +
                        "on u.id = mul.id_user"; //where id_user = ?
        String SQL_get_some_user = "select first_name, last_name, username, password from domain.users where id = ?";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL);
             PreparedStatement statement2 = connection.prepareStatement(SQL_messages_users_link);
             PreparedStatement statement3 = connection.prepareStatement(SQL_get_some_user)
        ){
            statement.setInt(1, pageSize);
            statement.setInt(2, pageOffset);
            ResultSet resultSet1 = statement.executeQuery();
            while (resultSet1.next()) {
                Long id = resultSet1.getLong("id");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet1.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                Long fromID = resultSet1.getLong("from");
                statement3.setLong(1,fromID);
                ResultSet resultSet3 = statement3.executeQuery();
                User from = null;
                while(resultSet3.next()) {
                    String firstName = resultSet3.getString("first_name");
                    String last_name = resultSet3.getString("last_name");
                    String username1 = resultSet3.getString("username");
                    String password = resultSet3.getString("password");
                    from = new User(firstName, last_name, username1, password);
                }
                from.setId(fromID);
                String txt = resultSet1.getString("text");
                Long replyToId = resultSet1.getLong("reply_to_id");
                Message replyToMessage = null;
                if(replyToId != 0)
                    replyToMessage = findOne(replyToId);
                List<User> to = new ArrayList<>();
                statement2.setLong(1,id);
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next()){
                    String toFirstName = resultSet2.getString("first_name");
                    String toLastName = resultSet2.getString("last_name");
                    String toUsername = resultSet2.getString("username");
                    String toPassword = resultSet2.getString("password");
                    Long toId = resultSet2.getLong("id_user");
                    User toUser = new User(toFirstName,toLastName, toUsername, toPassword);
                    toUser.setId(toId);
                    to.add(toUser);
                }
                Message message = new Message(from, to, txt);
                message.setId(id);
                message.setDate(dateTime);
                message.setReplyTo(replyToMessage);
                messages.add(message);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable,messages.stream());
    }


    public Page<Message> userPagingSentMessages(Pageable pageable, Long idUser){
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Message> messages = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL1 = "Select * from domain.messages where \"from\" = ? order by id limit ? offset ?";
        String SQL_messages_users_link =
                "select distinct id_user, first_name, last_name, username, password " +
                        "from domain.messages INNER JOIN domain.messages_users_link mul " +
                        "on ? = mul.id_message INNER JOIN domain.users u " +
                        "on u.id = mul.id_user";
        String SQL_get_some_user = "select first_name, last_name, username, password from domain.users where id = ?";
        try (PreparedStatement statement = connection.prepareStatement(SQL1);
             PreparedStatement statement2 = connection.prepareStatement(SQL_messages_users_link);
             PreparedStatement statement3 = connection.prepareStatement(SQL_get_some_user)
        ){
            //find all messages sent by user
            statement.setLong(1,idUser);
            statement.setInt(2, pageSize);
            statement.setInt(3, pageOffset);
            ResultSet resultSet1 = statement.executeQuery();
            while (resultSet1.next()) {
                Long id = resultSet1.getLong("id");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet1.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                Long fromID = resultSet1.getLong("from");
                statement3.setLong(1,fromID);
                ResultSet resultSet3 = statement3.executeQuery();
                User from = null;
                while(resultSet3.next()) {
                    String firstName = resultSet3.getString("first_name");
                    String last_name = resultSet3.getString("last_name");
                    String username1 = resultSet3.getString("username");
                    String password = resultSet3.getString("password");
                    from = new User(firstName, last_name, username1, password);
                }
                from.setId(fromID);
                String txt = resultSet1.getString("text");
                Long replyToId = resultSet1.getLong("reply_to_id");
                Message replyToMessage = null;
                if(replyToId != 0)
                    replyToMessage = findOne(replyToId);
                List<User> to = new ArrayList<>();
                statement2.setLong(1,id);
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next()){
                    String toFirstName = resultSet2.getString("first_name");
                    String toLastName = resultSet2.getString("last_name");
                    String toUsername = resultSet2.getString("username");
                    String toPassword = resultSet2.getString("password");
                    Long toId = resultSet2.getLong("id_user");
                    User toUser = new User(toFirstName,toLastName, toUsername, toPassword);
                    toUser.setId(toId);
                    to.add(toUser);
                }
                Message message = new Message(from, to, txt);
                message.setId(id);
                message.setDate(dateTime);
                message.setReplyTo(replyToMessage);
                messages.add(message);
            }



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return new PageImplementation<>(pageable,messages.stream());
    }

    public Page<Message> userPagingReceivedMessages(Pageable pageable, Long idUser){
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Message> messages = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select id_message from domain.messages_users_link where id_user = ? order by id_user limit ? offset ?";
        try(
        PreparedStatement statement = connection.prepareCall(SQL)) {
            statement.setLong(1,idUser);
            statement.setInt(2,pageSize);
            statement.setInt(3,pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Long idMessage = resultSet.getLong("id_message");
                Message message = findOne(idMessage);
                messages.add(message);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable,messages.stream());
    }
}
