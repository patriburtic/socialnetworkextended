package socialnetwork.repository.database;

import socialnetwork.domain.Connection;
import socialnetwork.domain.User;
import utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

public class ConnectionsRepositoryDB {
    private final String url;
    private final String username;
    private final String password;
    private final UserRepositoryDB userRepo;
    private java.sql.Connection connection;


    public ConnectionsRepositoryDB(String url, String username, String password, UserRepositoryDB userRepo) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.userRepo = userRepo;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Connection findOne(Long idUser, LocalDate date){
        Connection foundConnection = null;
        String SQL = "select id from domain.connections where id_user =? and date = ?";
        try(
                PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setLong(1,idUser);
            statement.setDate(2, Date.valueOf(date));
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                foundConnection = new Connection(userRepo.findOne(idUser));
                foundConnection.setId(id);
                foundConnection.setDate(date);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return foundConnection;
    }

    public Iterable<Connection> findAll(){
        Set<Connection> connections= new HashSet<>();
        String SQL = "select * from domain.connections";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
            ResultSet resultSet = statement.executeQuery()) {
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                Long idUser = resultSet.getLong("id_user");
                LocalDate date = LocalDate.parse(resultSet.getDate("date").toString(), Constants.DATE_FORMATTER1);

                User user = userRepo.findOne(idUser);
                Connection c = new Connection(user);
                c.setId(id);
                c.setDate(date);
                connections.add(c);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connections;
    }


    public Connection save(Connection c) {
        String SQL = "insert into domain.connections values (?,?,?)";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setLong(1,c.getId());
            statement.setLong(2,c.getUser().getId());
            statement.setDate(3,Date.valueOf(c.getDate().format(Constants.DATE_FORMATTER1)));
            statement.executeUpdate();
            return null;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return c;
    }


}
