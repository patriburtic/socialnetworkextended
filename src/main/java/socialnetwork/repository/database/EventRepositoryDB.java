package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventRepositoryDB implements PagingRepository<Long, Event> {
    private String url;
    private String username;
    private String password;
    private Validator<Event> validator;
    private Connection connection;

    public EventRepositoryDB(String url, String username, String password, Validator<Event> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Event findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        Event event = null;
        String SQL = "SELECT id, title, description, location, start_date, finish_date, id_creator FROM domain.events where id = ?";
        String SQL_participants_id = "SELECT id_user, notification FROM domain.events_users_link where id_event = ?";
        String SQL_participants = "SELECT * FROM domain.users where id = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
            PreparedStatement statement1 = connection.prepareStatement(SQL_participants_id);
            PreparedStatement statement2 = connection.prepareStatement(SQL_participants)) {
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDateTime startDate = LocalDateTime.parse(resultSet.getTimestamp("start_date").toString(), Constants.DATE_TIME_FORMATTER);
                LocalDateTime finishDate = LocalDateTime.parse(resultSet.getTimestamp("finish_date").toString(), Constants.DATE_TIME_FORMATTER);
                Long idCreator = resultSet.getLong("id_creator");
                statement2.setLong(1,idCreator);
                ResultSet resultSet1 = statement2.executeQuery();
                User creator = null;
                while(resultSet1.next()){
                    Long id_user = resultSet1.getLong("id");
                    String firstName = resultSet1.getString("first_name");
                    String lastName = resultSet1.getString("last_name");
                    String username = resultSet1.getString("username");
                    String password = resultSet1.getString("password");
                    creator = new User(firstName,lastName,username,password);
                    creator.setId(id_user);
                }
                event = new Event(title,description,location,startDate,finishDate);
                event.setId(id);
                event.setCreator(creator);
            }
            statement1.setLong(1,aLong);
            ResultSet resultSet1 = statement1.executeQuery();
            List<Tuple<Long,Boolean>> participantsIDs = new ArrayList<>();
            while(resultSet1.next()){
                Long id = resultSet1.getLong("id_user");
                Boolean notification = resultSet1.getBoolean("notification");
                participantsIDs.add(new Tuple<Long, Boolean>(id,notification));
            }
            if(participantsIDs.size() != 0){
                for(Tuple<Long,Boolean> id: participantsIDs){
                    statement2.setLong(1,id.getLeft());
                    ResultSet resultSet2 = statement2.executeQuery();
                    while(resultSet2.next()){
                        Long id_user = resultSet2.getLong("id");
                        String firstName = resultSet2.getString("first_name");
                        String lastName = resultSet2.getString("last_name");
                        String username = resultSet2.getString("username");
                        String password = resultSet2.getString("password");
                        User user = new User(firstName,lastName,username,password);
                        user.setId(id_user);
                        assert event != null;
                        event.addParticipant(user,id.getRight());
                    }
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return event;
    }

    @Override
    public Iterable<Event> findAll() {
        Set<Event> events = new HashSet<Event>();
        String SQL_events = "select * from domain.events";
        String SQL_creator = "SELECT * FROM domain.users where id = ?";
        String SQL_participants =
                "select distinct id_user, first_name, last_name, username, password, eul.notification "+
                        "from domain.events inner join domain.events_users_link eul "+
                        "on ? = eul.id_event inner join domain.users u "+
                        "on u.id = eul.id_user";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL_events);
            PreparedStatement statement1 = connection.prepareStatement(SQL_participants);
            PreparedStatement statement2 = connection.prepareStatement(SQL_creator);
            ResultSet resultSet = statement.executeQuery()
        ) {
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDateTime startDate = LocalDateTime.parse(resultSet.getTimestamp("start_date").toString(), Constants.DATE_TIME_FORMATTER);
                LocalDateTime finishDate = LocalDateTime.parse(resultSet.getTimestamp("finish_date").toString(), Constants.DATE_TIME_FORMATTER);
                Long idCreator = resultSet.getLong("id_creator");
                statement2.setLong(1,idCreator);
                ResultSet res = statement2.executeQuery();
                User creator = null;
                while(res.next()){
                    Long id_user = res.getLong("id");
                    String firstName = res.getString("first_name");
                    String lastName = res.getString("last_name");
                    String username = res.getString("username");
                    String password = res.getString("password");
                    creator = new User(firstName,lastName,username,password);
                    creator.setId(id_user);
                }
                Event event = new Event(title,description,location,startDate,finishDate);
                event.setId(id);
                event.setCreator(creator);
                statement1.setLong(1,id);
                ResultSet resultSet1 = statement1.executeQuery();
                while(resultSet1.next()){
                    Long id_user = resultSet1.getLong("id_user");
                    String firstName = resultSet1.getString("first_name");
                    String lastName = resultSet1.getString("last_name");
                    String username = resultSet1.getString("username");
                    String password = resultSet1.getString("password");
                    Boolean notification = resultSet1.getBoolean("notification");
                    User user = new User(firstName,lastName,username,password);
                    user.setId(id_user);
                    event.addParticipant(user,notification);
                }
                events.add(event);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return events;
    }

    @Override
    public Event save(Event entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);
        String SQL_event = "insert into domain.events values (?, ?, ?, ?, ?, ?, ?)";
        String SQL_users = "insert into domain.events_users_link values (?, ?, ?)";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL_event);
            PreparedStatement statement1 = connection.prepareStatement(SQL_users);
        ) {
            statement.setLong(1,entity.getId());
            statement.setString(2,entity.getTitle());
            statement.setString(3,entity.getDescription());
            statement.setString(4,entity.getLocation());
            statement.setTimestamp(5,Timestamp.valueOf(entity.getStartDate().format(Constants.DATE_TIME_FORMATTER)));
            statement.setTimestamp(6,Timestamp.valueOf(entity.getFinishDate().format(Constants.DATE_TIME_FORMATTER)));
            statement.setLong(7,entity.getCreator().getId());
            statement.executeUpdate();

            if(entity.getParticipants().size() != 0){
                for(Tuple<User,Boolean> participant: entity.getParticipants()) {
                    statement1.setLong(1, entity.getId());
                    statement1.setLong(2, participant.getLeft().getId());
                    statement1.setBoolean(3, participant.getRight());
                    statement1.executeUpdate();
                }
            }

            return null;


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

    @Override
    public Event delete(Long aLong) {
        if (aLong == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL_event = "delete from domain.events where id = ?";
        String SQL_link = "delete from domain.events_users_link where id_event = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL_link);
            PreparedStatement statement1 = connection.prepareStatement(SQL_event);
        ) {
            Event event = findOne(aLong);
            statement.setLong(1,aLong);
            statement.executeUpdate();
            statement1.setLong(1,aLong);
            statement1.executeUpdate();

            return event;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public Event update(Event entity) {
        return null;
    }

    public Event addParticipant(Long idEvent, Long idParticipant){
        if (idEvent== null)
            throw new IllegalArgumentException("ID must be not null");
        if (idParticipant== null)
            throw new IllegalArgumentException("ID must be not null");
        Event event = null;
        String SQL = "insert into domain.events_users_link values (?, ?, ?)";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
            statement.setLong(1,idEvent);
            statement.setLong(2,idParticipant);
            statement.setBoolean(3,true);
            statement.executeUpdate();
            event = findOne(idEvent);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return event;
    }


    public Event updateParticipant(Long idEvent, Long idUser, Boolean notificationChoice){
        if (idEvent== null)
            throw new IllegalArgumentException("ID must be not null");
        if (idUser== null)
            throw new IllegalArgumentException("ID must be not null");
        Event event = null;
        String SQL = "update domain.events_users_link set notification = ? where id_event =? and id_user = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
            statement.setLong(2,idEvent);
            statement.setLong(3,idUser);
            statement.setBoolean(1,notificationChoice);
            statement.executeUpdate();
            event = findOne(idEvent);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return event;
    }


    public Event deleteParticipant(Long idEvent, Long idParticipant){
        if (idEvent== null)
            throw new IllegalArgumentException("ID must be not null");
        if (idParticipant== null)
            throw new IllegalArgumentException("ID must be not null");
        Event event = null;
        String SQL = "delete from domain.events_users_link where id_event = ? and id_user = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
            event = findOne(idEvent);
            statement.setLong(1,idEvent);
            statement.setLong(2,idParticipant);
            statement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return event;
    }

    public Page<Event> usersParticipatingEvents(Pageable pageable, Long idUser){
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Event> events = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select id_event from domain.events_users_link where id_user = ? order by id_user limit ? offset ?";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL)){
            statement.setLong(1,idUser);
            statement.setInt(2,pageSize);
            statement.setInt(3,pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Long idEvent = resultSet.getLong("id_event");
                Event event = findOne(idEvent);
                events.add(event);
            }

        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable,events.stream());
    }


    @Override
    public Page<Event> findAll(Pageable pageable) {
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Event> events = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select * from domain.events order by id limit ? offset ?";
        String SQL_creator = "Select * from domain.users where id = ?";
        String SQL_participants =
                "select distinct id_user, first_name, last_name, username, password, eul.notification "+
                        "from domain.events inner join domain.events_users_link eul "+
                        "on ? = eul.id_event inner join domain.users u "+
                        "on u.id = eul.id_user";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL);
             PreparedStatement statement2 = connection.prepareStatement(SQL_creator);
             PreparedStatement statement1 = connection.prepareStatement(SQL_participants)){
            statement.setInt(1, pageSize);
            statement.setInt(2, pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDateTime startDate = LocalDateTime.parse(resultSet.getTimestamp("start_date").toString(), Constants.DATE_TIME_FORMATTER);
                LocalDateTime finishDate = LocalDateTime.parse(resultSet.getTimestamp("finish_date").toString(), Constants.DATE_TIME_FORMATTER);
                Long idCreator = resultSet.getLong("id_creator");
                statement2.setLong(1,idCreator);
                User creator = null;
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next()){
                    Long id_user = resultSet2.getLong("id");
                    String firstName = resultSet2.getString("first_name");
                    String lastName = resultSet2.getString("last_name");
                    String username = resultSet2.getString("username");
                    String password = resultSet2.getString("password");
                    creator = new User(firstName, lastName, username, password);
                    creator.setId(id_user);
                }
                Event event = new Event(title,description,location,startDate,finishDate);
                event.setCreator(creator);
                event.setId(id);
                statement1.setLong(1,id);
                ResultSet resultSet1 = statement1.executeQuery();
                while(resultSet1.next()){
                    Long id_user = resultSet1.getLong("id_user");
                    String firstName = resultSet1.getString("first_name");
                    String lastName = resultSet1.getString("last_name");
                    String username = resultSet1.getString("username");
                    String password = resultSet1.getString("password");
                    Boolean notification = resultSet1.getBoolean("notification");
                    User user = new User(firstName,lastName,username,password);
                    user.setId(id_user);
                    event.addParticipant(user,notification);
                }
                events.add(event);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<Event>(pageable,events.stream());
//        Paginator<Event> paginator = new Paginator<>(pageable,findAll());
//        return paginator.paginate();
    }
}
