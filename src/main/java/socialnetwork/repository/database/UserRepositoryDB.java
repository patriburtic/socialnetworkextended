package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * UserRepositoryDB implements the Repository interface
 * this is a repository that collects the data from database
 */
public class UserRepositoryDB implements PagingRepository<Long, User> {
    private String url;
    private String username;
    private String password;
    private Validator<User> validator;
    private Connection connection;

    /**
     *
     * @param url - the link to database
     * @param username - required to connect to the database
     * @param password - required to connect to the database
     * @param validator - validates the data
     */
    public UserRepositoryDB(String url, String username, String password, Validator<User> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.connection = DriverManager.getConnection(url,this.username,password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * @throws IllegalArgumentException if the ID is null
     * @param aLong - id of the entity we want to find
     *              ID must be not null!
     * @return the entity that has the id equal to aLong if found, null otherwise
     */
    @Override
    public User findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        User user = null;
        String SQL_find_user = "SELECT id, first_name, last_name, username, password FROM domain.users WHERE id = ? ";
        try(PreparedStatement statement = connection.prepareStatement(SQL_find_user)){
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                user = new User(firstName,lastName,username,password);
                user.setId(id);
                user.setPassword(password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     *
     * @return all users
     */
    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        String SQL = "select * from domain.users";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                User user = new User(firstName,lastName,username,password);
                user.setId(id);
                user.setPassword(password);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
    /**
     *
     * @param entity -User
     *         entity must be not null
     * @return null- if the given entity is saved
     *         otherwise returns the entity (id already exists)
     * @throws ValidationException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.     *
     */
    @Override
    public User save(User entity) {
        if (entity==null)
            throw new IllegalArgumentException("Entity must be not null");
        if(findByUsername(entity.getUsername()) != null)
            throw new IllegalArgumentException("Exista deja un utilizator cu acest username.");
        validator.validate(entity);
        String SQL = "insert into domain.users values (?, ?, ?, ?, ?)";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            statement.setLong(1, entity.getId());
            statement.setString(2, entity.getFirstName());
            statement.setString(3, entity.getLastName());
            statement.setString(4, entity.getUsername());
            statement.setString(5, entity.getPassword());
            statement.executeUpdate();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }
    /**
     *  removes the entity with the specified id
     * @param aLong
     *      id must be not null
     * @return the removed entity or null if there is no entity with the given id
     * @throws IllegalArgumentException
     *                   if the given id is null.
     */
    @Override
    public User delete(Long aLong) {
        if (aLong == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL = "Delete from domain.users where id = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            User user = findOne(aLong);
            statement.setLong(1,aLong);
            statement.executeUpdate();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     *
     * @param entity
     *          entity must not be null
     * @return null - if the entity is updated,
     *                otherwise  returns the entity  - (e.g id does not exist).
     * @throws IllegalArgumentException
     *             if the given entity is null.
     * @throws ValidationException
     *             if the entity is not valid.
     */
    @Override
    public User update(User entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);
        String SQL = "Update domain.users set first_name = ?, last_name = ?, username = ?, password = ? where id = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3,entity.getUsername());
            statement.setString(4,entity.getPassword());
            statement.setLong(5,entity.getId());
            statement.executeUpdate();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    public User findByUsername(String username){
        if(username == null)
            throw new IllegalArgumentException("Username must be not null!\n");
        User user = null;
        String SQL_find_user = "SELECT id, first_name, last_name, password FROM domain.users WHERE username = ? ";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL_find_user)){
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");
                user = new User(firstName,lastName,username,password);
                user.setId(id);
                user.setPassword(password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;

    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<User> users = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select * from domain.users order by id limit ? offset ?";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setInt(1, pageSize);
            statement.setInt(2, pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = null;
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                user = new User(firstName, lastName, username, password);
                user.setId(id);
                user.setPassword(password);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new PageImplementation<User>(pageable, users.stream());
    }

    }