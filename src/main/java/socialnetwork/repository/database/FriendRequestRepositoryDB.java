package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.utils.Status;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class FriendRequestRepositoryDB implements PagingRepository<Tuple<Long,Long>, FriendRequest> {
    private String url;
    private String username;
    private String password;
    private Validator<FriendRequest> validator;
    private Connection connection;

    /**
     * @param url       - the link to database
     * @param username  - required to connect to the database
     * @param password  - required to connect to the database
     * @param validator - validates the data
     */
    public FriendRequestRepositoryDB(String url, String username, String password, Validator<FriendRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * @param longLongTuple -the id of the entity to be returned
     *                      id must not be null
     * @return the entity with the specified id
     * or null - if there is no entity with the given id
     * @throws IllegalArgumentException if id is null.
     */
    @Override
    public FriendRequest findOne(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        FriendRequest friendRequest = null;
        String SQL = "Select id_from, id_to, date, status FROM domain.friend_requests" +
                " where id_from = ? and id_to = ?" +
                " or id_from = ? and id_to = ?";
        try (

                PreparedStatement statement = connection.prepareStatement(SQL)
        ) {
            statement.setLong(1, longLongTuple.getLeft());
            statement.setLong(2, longLongTuple.getRight());
            statement.setLong(3, longLongTuple.getRight());
            statement.setLong(4, longLongTuple.getLeft());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long fromId = resultSet.getLong("id_from");
                Long toId = resultSet.getLong("id_to");
                Tuple<Long, Long> id = new Tuple<>(fromId, toId);
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                int statusInt = resultSet.getInt("status");
                Status status = switch (statusInt) {
                    case 0 -> Status.PENDING;
                    case 1 -> Status.APPROVED;
                    case 2 -> Status.REJECTED;
                    default -> Status.UNKNOWN;
                };
                friendRequest = new FriendRequest();
                friendRequest.setId(id);
                friendRequest.setDate(dateTime);
                friendRequest.setStatus(status);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return friendRequest;
    }

    /**
     * @return all friend requests
     */
    @Override
    public Iterable<FriendRequest> findAll() {
        Set<FriendRequest> friendRequests = new HashSet<>();
        String SQL = "select * from domain.friend_requests";
        try (

                PreparedStatement statement = connection.prepareStatement(SQL);
                ResultSet resultSet = statement.executeQuery()
        ) {
            while (resultSet.next()) {
                Long idFrom = resultSet.getLong("id_from");
                Long idTo = resultSet.getLong("id_to");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                int statusInt = resultSet.getInt("status");
                Status status = switch (statusInt) {
                    case 0 -> Status.PENDING;
                    case 1 -> Status.APPROVED;
                    case 2 -> Status.REJECTED;
                    default -> Status.UNKNOWN;
                };
                FriendRequest friendRequest = new FriendRequest();
                friendRequest.setId(new Tuple<Long, Long>(idFrom, idTo));
                friendRequest.setDate(dateTime);
                friendRequest.setStatus(status);
                friendRequests.add(friendRequest);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return friendRequests;
    }

    /**
     * @param entity entity must be not null
     * @return null- if the given entity is saved
     * otherwise returns the entity (id already exists)
     * @throws ValidationException      if the entity is not valid
     * @throws IllegalArgumentException if the given entity is null.
     */
    @Override
    public FriendRequest save(FriendRequest entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null!");
        String SQL = "insert into domain.friend_requests values (?, ?, ?, ?)";
        validator.validate(entity);
        try (

                PreparedStatement statement = connection.prepareStatement(SQL)
        ) {
            statement.setLong(1, entity.getId().getLeft());
            statement.setLong(2, entity.getId().getRight());
            statement.setTimestamp(3, Timestamp.valueOf(entity.getDate().format(Constants.DATE_TIME_FORMATTER)));
            int statusID = switch (entity.getStatus()) {
                case PENDING -> 0;
                case APPROVED -> 1;
                case REJECTED -> 2;
                case UNKNOWN -> 3;
            };
            statement.setInt(4, statusID);
            statement.executeUpdate();
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

    /**
     * removes the entity with the specified id
     *
     * @param longLongTuple id must be not null
     * @return the removed entity or null if there is no entity with the given id
     * @throws IllegalArgumentException if the given id is null.
     */
    @Override
    public FriendRequest delete(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL = "Delete from domain.friend_requests " +
                "where id_from = ? and id_to =? " +
                "or id_from = ? and id_to = ?";
        try (

                PreparedStatement statement = connection.prepareStatement(SQL)
        ) {
            FriendRequest friendRequest = findOne(longLongTuple);
            statement.setLong(1, longLongTuple.getLeft());
            statement.setLong(2, longLongTuple.getRight());
            statement.setLong(3, longLongTuple.getRight());
            statement.setLong(4, longLongTuple.getLeft());
            statement.executeUpdate();
            return friendRequest;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest update(FriendRequest entity) {
        return null;
    }

    @Override
    public Page<FriendRequest> findAll(Pageable pageable) {
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, findAll());
        return paginator.paginate();
    }

    public Page<FriendRequest> usersPagingFriendRequests(Pageable pageable, Long idUser) {
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<FriendRequest> friendRequests = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select * from domain.friend_requests where id_to =? order by id_to limit ? offset ?";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setLong(1, idUser);
            statement.setInt(2, pageSize);
            statement.setLong(3, pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long idFrom = resultSet.getLong("id_from");
                Tuple<Long, Long> id = new Tuple<>(idFrom, idUser);
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                int statusInt = resultSet.getInt("status");
                Status status = switch (statusInt) {
                    case 0 -> Status.PENDING;
                    case 1 -> Status.APPROVED;
                    case 2 -> Status.REJECTED;
                    default -> Status.UNKNOWN;
                };
                FriendRequest friendRequest = new FriendRequest();
                friendRequest.setId(id);
                friendRequest.setDate(dateTime);
                friendRequest.setStatus(status);
                friendRequests.add(friendRequest);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable,friendRequests.stream());
    }
}
