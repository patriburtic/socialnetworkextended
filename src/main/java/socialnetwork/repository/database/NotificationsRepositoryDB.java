package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.*;
import utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class NotificationsRepositoryDB {

    private final String url;
    private final String username;
    private final String password;
    private final EventRepositoryDB eventRepo;
    private final UserRepositoryDB userRepo;
    private Connection connection;

    public NotificationsRepositoryDB(String url, String username, String password, EventRepositoryDB eventRepo, UserRepositoryDB userRepo) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.eventRepo = eventRepo;
        this.userRepo = userRepo;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Page<Notification> findAll(Pageable pageable, Long idUser) {
//        Paginator<Notification> paginator = new Paginator<>(pageable,findAllNotifications(idUser));
//        return paginator.paginate();
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Notification> notifications = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "Select * from domain.notifications where id_user =? order by id limit ? offset ?";
        try (
             PreparedStatement notificationStatement = connection.prepareStatement(SQL)) {
            notificationStatement.setLong(1,idUser);
            notificationStatement.setInt(2, pageSize);
            notificationStatement.setInt(3, pageOffset);
            ResultSet resultSet = notificationStatement.executeQuery();
            User user = userRepo.findOne(idUser);
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long idEvent = resultSet.getLong("id_event");
                String message = resultSet.getString("message");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Event event = eventRepo.findOne(idEvent);

                Notification notification = new Notification(event, user, message, dateTime);
                notification.setId(id);
                notifications.add(notification);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable, notifications.stream());
    }


    public Iterable<Notification> findAll() {
        Set<Notification> notifications = new HashSet<>();
        String SQL = "Select id, id_event, id_user, message,date from domain.notifications";
        try (
             PreparedStatement notificationStatement = connection.prepareStatement(SQL)
        ) {
            ResultSet resultSet = notificationStatement.executeQuery();
            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long idEvent = resultSet.getLong("id_event");
                Long idUser = resultSet.getLong("id_user");
                String message = resultSet.getString("message");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Event event = eventRepo.findOne(idEvent);
                User user = userRepo.findOne(idUser);

                Notification notification = new Notification(event, user, message, dateTime);
                notification.setId(id);
                notifications.add(notification);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return notifications;
    }

        public Set<Notification> findAllNotifications(Long idUser) {
        if(idUser == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        Set<Notification> notifications = new HashSet<>();
        String SQL = "Select id, id_event, message, date from domain.notifications where id_user = ?";
        try(
            PreparedStatement notificationStatement = connection.prepareStatement(SQL);
        ) {
            Notification notification = null;
            notificationStatement.setLong(1,idUser);
            ResultSet notificationResult = notificationStatement.executeQuery();
            while(notificationResult.next()){
                Long id = notificationResult.getLong("id");
                Long idEvent = notificationResult.getLong("id_event");
                String message = notificationResult.getString("message");
                LocalDateTime date = LocalDateTime.parse(notificationResult.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Event event = eventRepo.findOne(idEvent);
                User user = userRepo.findOne(idUser);

                if(event.getParticipants().stream().anyMatch(x->x.getLeft().equals(user))) {

                    notification = new Notification(event, user, message, date);
                    notification.setId(id);
                    notifications.add(notification);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return notifications;
    }

    public Notification findOne(Long id){
        if(id == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        Notification notification = null;
        String SQL = "Select id_event, id_user, message, date, id_event, message, date from domain.notifications where id = ?";
        try(
            PreparedStatement notificationStatement = connection.prepareStatement(SQL);
        ) {
            notificationStatement.setLong(1,id);
            ResultSet notificationResult = notificationStatement.executeQuery();
            while(notificationResult.next()){
                Long idUser = notificationResult.getLong("id_user");
                Long idEvent = notificationResult.getLong("id_event");
                String message = notificationResult.getString("message");
                LocalDateTime date = LocalDateTime.parse(notificationResult.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Event event = eventRepo.findOne(idEvent);
                User user = userRepo.findOne(idUser);

                notification = new Notification(event, user, message, date);
                notification.setId(id);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return notification;

    }


    public Notification save(Notification entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        String SQL = "insert into domain.notifications values (?,?,?,?,?)";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
            statement.setLong(1, entity.getId());
            statement.setLong(2, entity.getEvent().getId());
            statement.setLong(3, entity.getUser().getId());
            statement.setString(4, entity.getMessage());
            statement.setTimestamp(5, Timestamp.valueOf(entity.getDate().format(Constants.DATE_TIME_FORMATTER)));

            statement.executeUpdate();

            return null;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return entity;
    }


    public Notification delete(Long aLong) {
        if (aLong == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL = "delete from domain.notifications where id = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
            Notification notification = findOne(aLong);
            statement.setLong(1,aLong);
            statement.executeUpdate();

            return notification;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
