package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import utils.Constants;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class FriendshipRepositoryDB implements PagingRepository<Tuple<Long,Long>, Friendship> {
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;
    private Connection connection;
    /**
     *
     * @param url - the link to database
     * @param username - required to connect to the database
     * @param password - required to connect to the database
     * @param validator - validates the data
     */
    public FriendshipRepositoryDB(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    /**
     *
     * @param longLongTuple -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     *          or null - if there is no entity with the given id
     * @throws IllegalArgumentException
     *                  if id is null.
     */
    @Override
    public Friendship findOne(Tuple<Long, Long> longLongTuple) {
        if(longLongTuple == null)
            throw new IllegalArgumentException("ID must be not null!\n");
        Friendship friendship = null;
        String SQL = "SELECT id1, id2, date FROM domain.friendships WHERE id1 = ? and id2 =? or id1 = ? and id2 = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            statement.setLong(1, longLongTuple.getLeft());
            statement.setLong(2, longLongTuple.getRight());
            statement.setLong(4, longLongTuple.getLeft());
            statement.setLong(3, longLongTuple.getRight());
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                Tuple<Long,Long> id = new Tuple<>(id1,id2);
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);
                friendship = new Friendship();
                friendship.setId(id);
                friendship.setDate(dateTime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendship;
    }

    /**
     *
     * @return all friendships
     */
    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        String SQL = "select * from domain.friendships";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL);
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Friendship friendship = new Friendship();
                friendship.setId(new Tuple<Long,Long>(id1,id2));
                friendship.setDate(dateTime);
                friendships.add(friendship);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }
    /**
     *
     * @param entity
     *         entity must be not null
     * @return null- if the given entity is saved
     *         otherwise returns the entity (id already exists)
     * @throws ValidationException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     */
    @Override
    public Friendship save(Friendship entity) {
        if (entity==null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);
        String SQL = "insert into domain.friendships values (?, ?, ?)";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            statement.setLong(1, entity.getId().getLeft());
            statement.setLong(2, entity.getId().getRight());
            statement.setTimestamp(3, Timestamp.valueOf(entity.getDate().format(Constants.DATE_TIME_FORMATTER)));
            statement.executeUpdate();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }
    /**
     *  removes the entity with the specified id
     * @param longLongTuple
     *      id must be not null
     * @return the removed entity or null if there is no entity with the given id
     * @throws IllegalArgumentException
     *                   if the given id is null.
     */
    @Override
    public Friendship delete(Tuple<Long, Long> longLongTuple) {
        if (longLongTuple == null)
            throw new IllegalArgumentException("ID must be not null");
        String SQL = "Delete from domain.friendships where id1 = ? and id2 =? or id1 = ? and id2 = ?";
        try(
            PreparedStatement statement = connection.prepareStatement(SQL)){
            Friendship friendship = findOne(longLongTuple);
            statement.setLong(1,longLongTuple.getRight());
            statement.setLong(2,longLongTuple.getLeft());
            statement.setLong(3,longLongTuple.getLeft());
            statement.setLong(4,longLongTuple.getRight());
            statement.executeUpdate();
            return friendship;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship update(Friendship entity) {
        return entity;
    }

    @Override
    public Page<Friendship> findAll(Pageable pageable) {
        Paginator<Friendship> paginator = new Paginator<>(pageable,findAll());
        return paginator.paginate();
    }

    public Page<Friendship> getUserFriends(Pageable pageable, Long idUser){
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Set<Friendship> friendships = new HashSet<>();
        int pageOffset = (pageNumber - 1) * pageSize;
        String SQL = "select * from domain.friendships where id1 = ? or id2 = ? limit ? offset ?";
        try (
             PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setLong(1,idUser);
            statement.setLong(2,idUser);
            statement.setInt(3,pageSize);
            statement.setLong(4,pageOffset);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getTimestamp("date").toString(), Constants.DATE_TIME_FORMATTER);

                Friendship friendship = new Friendship();
                friendship.setId(new Tuple<Long,Long>(id1,id2));
                friendship.setDate(dateTime);
                friendships.add(friendship);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PageImplementation<>(pageable,friendships.stream());
    }
}
