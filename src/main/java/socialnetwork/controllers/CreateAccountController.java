package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Stack;

public class CreateAccountController implements Controller{
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password1;
    @FXML
    private PasswordField password2;
    PageService service;
    Stage stage;

    @Override
    public void initialize() {

    }

    public void goToLogin(ActionEvent actionEvent) throws IOException {
        stage.close();
        FXMLLoader loaderLogin = new FXMLLoader();
        loaderLogin.setLocation(getClass().getResource("/view/loginViewNew.fxml"));
        Pane loginRoot = loaderLogin.load();
        LoginController loginController = loaderLogin.getController();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(loginRoot, 600, 400));
        primaryStage.setTitle("Bun venit!");
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setMaxWidth(600);
        loginController.setContext(service, primaryStage);
        primaryStage.getIcons().add(new Image("/images/Hypen.png"));
        primaryStage.show();
    }

    public void createNewAccount(ActionEvent actionEvent) {
        String firstNameString = firstName.getText();
        String lastNameString = lastName.getText();
        String usernameString = userName.getText();
        String password1String = password1.getText();
        String password2String = password2.getText();
        try {
            if(password1String.equals(""))
                throw new Exception("Introduceti o parola!\n");
            if(password2String.equals(""))
                throw new Exception("Introduceti din nou parola!\n");
            if (!password1String.equals(password2String))
                throw new Exception("UPS! Nu ai introdus aceeasi parola, te rugam sa verifici din nou!\n");

            service.addUser(firstNameString,lastNameString,usernameString,password1String);
            service.logIn(usernameString,password1String);
            stage.close();
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/homePageView.fxml"));
            BorderPane root=loader.load();
            Stage primaryStage = new Stage();
            primaryStage.setScene(new Scene(root, 700, 500));
            MainController ctrl=loader.getController();
            Stack<SceneType> undoStack = new Stack<>();
            ctrl.setContext(service,primaryStage,undoStack);
            primaryStage.setTitle("Link!");
            primaryStage.getIcons().add(new Image("/images/Hypen.png"));
            primaryStage.show();
            CustomMessageWindow.showMessage(null, Alert.AlertType.INFORMATION,"Bun venit!", "Bine ai venit in comunitatea noastra!");

        } catch (Exception e) {
            CustomMessageWindow.showErrorMessage(null, e.getMessage());
        }
    }

    public void setContext(PageService service, Stage stage){
        this.service = service;
        this.stage = stage;
    }
}
