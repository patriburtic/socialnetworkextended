package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.FriendshipDTO;
import socialnetwork.domain.User;
import socialnetwork.domain.MessageDTO1;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;
import utils.Constants;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Stack;

public class GeneratedReportController implements Controller{
    PageService service;
    Stack<SceneType> undoStack;
    User connectedUser;
    User friend;
    int reportType;
    LocalDateTime start;
    LocalDateTime finish;
    ObservableList<MessageDTO1> messagesModel = FXCollections.observableArrayList();
    ObservableList<FriendshipDTO> friendshipsModel = FXCollections.observableArrayList();
    @FXML
    Stage mainStage;
    @FXML
    Label friendshipsLabel;
    @FXML
    Label messagesLabel;
    @FXML
    Label periodLabel;
    @FXML
    TableView<FriendshipDTO> friendshipTable;
    @FXML
    TableView<MessageDTO1> messageTable;
    @FXML
    TableColumn<FriendshipDTO,String> completeNameColumn;
    @FXML
    TableColumn<FriendshipDTO,String> date1Column;
    @FXML
    TableColumn<MessageDTO1,String> fromColumn;
    @FXML
    TableColumn<MessageDTO1,String> toColumn;
    @FXML
    TableColumn<MessageDTO1,String> messageColumn;
    @FXML
    TableColumn<MessageDTO1,String> date2Column;
    @Override
    public void initialize() {
       completeNameColumn.setCellValueFactory(new PropertyValueFactory<FriendshipDTO,String>("completeName"));
       date1Column.setCellValueFactory(new PropertyValueFactory<FriendshipDTO,String>("date"));
       fromColumn.setCellValueFactory(new PropertyValueFactory<MessageDTO1,String>("from"));
       toColumn.setCellValueFactory(new PropertyValueFactory<MessageDTO1,String>("to"));
       messageColumn.setCellValueFactory(new PropertyValueFactory<MessageDTO1,String>("message"));
       date2Column.setCellValueFactory(new PropertyValueFactory<MessageDTO1,String>("date"));
       friendshipTable.setItems(friendshipsModel);
       messageTable.setItems(messagesModel);
       friendshipTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        completeNameColumn.minWidthProperty().bind(
                friendshipTable.widthProperty().multiply(0.5));

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack, User friend, int reportType, LocalDateTime start, LocalDateTime finish){
        this.service = service;
        this.connectedUser = service.getCurrentUser();
        this.undoStack = undoStack;
        this.mainStage = mainStage;
        this.reportType = reportType;
        this.friend = friend;
        this.start = start;
        this.finish = finish;
        initModel(reportType);
    }

    public void initModel(int reportType){
        periodLabel.setText("Perioada: "+start.format(Constants.DATE_FORMATTER1)+" - "+finish.format(Constants.DATE_FORMATTER1));
        switch (reportType) {
            case 1 -> {
                List<FriendshipDTO> friendshipDTOS = service.getFriendshipsFromPeriodDTO(start, finish);
                friendshipsLabel.setText(friendshipDTOS.size()+" prieteni noi.");
                friendshipsModel.setAll(friendshipDTOS);
                List<MessageDTO1> messageDTO1s = service.getMessagesFromPeriodDTO(null, start, finish);
                messagesModel.setAll(messageDTO1s);
                messagesLabel.setText(messageDTO1s.size()+" mesaje noi.");
            }
            case 2 -> {
                friendshipTable.setVisible(false);
                friendshipsLabel.setVisible(false);
                List<MessageDTO1> messageDTO1s = service.getMessagesFromPeriodDTO(friend, start, finish);
                messagesModel.setAll(messageDTO1s);
                messagesLabel.setText(messageDTO1s.size()+" mesaje noi.");
            }
        }

    }


    public void savePDF(ActionEvent actionEvent) {
        try {
            LocalDateTime startDate = start;
            LocalDateTime finishDate = finish;

            switch (reportType) {
                case 1:
                    try {
                        service.validateDates(start, finish);
                        DirectoryChooser directoryChooser = new DirectoryChooser();
                        directoryChooser.setInitialDirectory(new File("src"));
                        Stage newStage = new Stage();
                        File selectedDirectory = directoryChooser.showDialog(newStage);
                        String path = selectedDirectory.getAbsolutePath();
                        service.createFirstReport(startDate, finishDate, path);
                        CustomMessageWindow.showMessage(null, Alert.AlertType.INFORMATION, "Succes", "Raportul a fost salvat cu succes");
                    } catch (ComunityException e) {
                        CustomMessageWindow.showErrorMessage(null, e.getMessage());
                    }
                    break;

                case 2:
                    User user = friend;
                    try {
                        service.validateDates(startDate, finishDate);
                        if (user == null)
                            throw new ComunityException("Alegeti un utilizator.");
                        DirectoryChooser directoryChooser = new DirectoryChooser();
                        directoryChooser.setInitialDirectory(new File("src"));
                        Stage newStage = new Stage();
                        File selectedDirectory = directoryChooser.showDialog(newStage);
                        String path = selectedDirectory.getAbsolutePath();
                        service.validateDates(startDate, finishDate);
                        service.createSecondReport(user, startDate, finishDate, path);
                        CustomMessageWindow.showMessage(null, Alert.AlertType.INFORMATION, "Succes", "Raportul a fost salvat cu succes");
                    } catch (ComunityException e) {
                        CustomMessageWindow.showErrorMessage(null, e.getMessage());
                    }
                    break;
            }
        } catch (ComunityException e) {
            CustomMessageWindow.showErrorMessage(null, e.getMessage());
        }
    }

    @FXML
    public void goHome() throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }
}
