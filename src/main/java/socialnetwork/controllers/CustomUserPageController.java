package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;

import java.io.IOException;
import java.util.Stack;

public class CustomUserPageController implements Controller {
    User connectedUser;
    User thisUser;
    Stack<SceneType> undoStack;
    PageService service;
    boolean areFriends;
    boolean friendRequestAlreadySentToMe;
    boolean friendRequestAlreadySentToUser;
    boolean blocked;
    @FXML
    Stage mainStage;
    @FXML
    Label username;
    @FXML
    Button friendRequestBttn;
    @FXML
    Button removeFriendBttn;
    @FXML
    public void initialize(){

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack, User thisUser){
        this.service = service;
        this.connectedUser = service.getCurrentUser();
        this.thisUser = thisUser;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        username.setText(thisUser.getFirstName()+" "+thisUser.getLastName());
        manageBttns();

    }

    public void manageBttns(){
        areFriends = service.areFriends(connectedUser,thisUser);
        friendRequestAlreadySentToMe = service.friendRequestAlreadySent(thisUser,connectedUser);
        friendRequestAlreadySentToUser = service.friendRequestAlreadySent(connectedUser,thisUser);
        blocked = service.isRejected(connectedUser,thisUser);
        if(areFriends) {
            friendRequestBttn.setText("Prieteni");
            friendRequestBttn.setDisable(true);
            removeFriendBttn.setText("Sterge prieten");
            removeFriendBttn.setDisable(false);
        }
        else if(friendRequestAlreadySentToMe){
            friendRequestBttn.setText("Ti-a trimis cerere de prietenie");
            friendRequestBttn.setTooltip(new Tooltip("Ti-a trimis cerere de prietenie"));
            friendRequestBttn.setDisable(true);
            removeFriendBttn.setText("Sterge prieten");
            removeFriendBttn.setDisable(true);
        }
        else if(friendRequestAlreadySentToUser){
            friendRequestBttn.setText("Anuleaza cererea");
            friendRequestBttn.setDisable(false);
            removeFriendBttn.setText("Sterge prieten");
            removeFriendBttn.setDisable(true);
        }
        else if(blocked){
            friendRequestBttn.setText("Indisponibil");
            friendRequestBttn.setDisable(true);
            removeFriendBttn.setText("Sterge prieten");
            removeFriendBttn.setDisable(true);
        }
        else{
            friendRequestBttn.setText("Adauga prieten");
            friendRequestBttn.setDisable(false);
            removeFriendBttn.setText("Sterge prieten");
            removeFriendBttn.setDisable(true);
        }
    }

    public void goHome() throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }

    public void handleFriendRequest(ActionEvent actionEvent) {
        if(!friendRequestBttn.isDisabled()){
            if(!areFriends && !friendRequestAlreadySentToUser) {
                try {
                    service.sendFriendRequest(connectedUser, thisUser);
                    manageBttns();
                    Notifications notification = Notifications.create()
                            .title("SUCCES")
                            .text("Cererea de prietenie a fost trimisa.")
                            .graphic(null)
                            .hideAfter(Duration.seconds(2))
                            .position(Pos.TOP_CENTER)
                            .owner(mainStage);
                    notification.show();
                } catch (ComunityException exception) {
                    CustomMessageWindow.showErrorMessage(null, exception.getMessage());
                }
            }
            else if(friendRequestAlreadySentToUser){
                try{
                    service.removeSentFriendRequest(connectedUser,thisUser);
                    manageBttns();
                    Notifications notification = Notifications.create()
                            .title("SUCCES")
                            .text("Cererea de prietenie a fost anulata.")
                            .graphic(null)
                            .hideAfter(Duration.seconds(2))
                            .position(Pos.TOP_CENTER)
                            .owner(mainStage);
                    notification.show();
                }catch(ComunityException ce){
                    CustomMessageWindow.showErrorMessage(null,ce.getMessage());
                }
            }
        }
    }

    public void handleRemoveFriend(ActionEvent actionEvent) {
        if(!removeFriendBttn.isDisabled()){
            try{
                service.deleteFriendship(connectedUser.getId(), thisUser.getId());
                areFriends = false;
                Notifications notification = Notifications.create()
                        .title("Prietenie strearsa.")
                        .text("Tu si "+thisUser.getFirstName()+" "+thisUser.getLastName()+" nu mai sunteti prieteni.")
                        .graphic(null)
                        .hideAfter(Duration.seconds(3))
                        .position(Pos.TOP_CENTER)
                        .owner(mainStage);
                notification.show();
                manageBttns();
            }catch(ComunityException exception){
                CustomMessageWindow.showErrorMessage(null, exception.getMessage());
            }
        }
    }
}
