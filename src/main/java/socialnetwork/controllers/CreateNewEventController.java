package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Stack;

public class CreateNewEventController implements Controller {
    @FXML
    private TextArea description;
    @FXML
    private DatePicker finishDate;
    @FXML
    private TextField title;
    @FXML
    private TextField eventLocation;
    @FXML
    private DatePicker startDate;
    private PageService service;
    Stage mainStage;
    Stack<SceneType> undoStack;
    User connectedUser;
    @Override
    public void initialize() {
        
    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        this.connectedUser = service.getCurrentUser();
    }


    @FXML
    public void goHome() throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }


    public void createNewEvent(ActionEvent actionEvent) {
        String title = this.title.getText();
        String location = this.eventLocation.getText();
        String description = this.description.getText();
        LocalDate date1 = startDate.getValue();
        LocalDate date2 = finishDate.getValue();

        try{
            service.createNewEvent(title,location,description,date1,date2,connectedUser);
            Notifications notification = Notifications.create()
                    .title("SUCCES")
                    .text("Evenimentul a fost creeat cu succes.")
                    .graphic(null)
                    .hideAfter(Duration.seconds(3))
                    .position(Pos.TOP_CENTER)
                    .owner(mainStage);
            notification.show();
            this.title.setText("");
            this.eventLocation.setText("");
            this.description.setText("");
            this.startDate.setValue(null);
            this.finishDate.setValue(null);
        }catch(IllegalArgumentException | ValidationException | ComunityException e){
            CustomMessageWindow.showErrorMessage(null,e.getMessage());
        }

    }

    public void goBack(ActionEvent actionEvent) throws IOException {
        EventController ctrl = (EventController) SceneFactory.changeScene(SceneType.AllEvents,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }
}
