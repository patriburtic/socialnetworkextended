package socialnetwork.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import socialnetwork.Observer.Observer;
import socialnetwork.controllers.tools.EventPane;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class EventController implements Controller, Observer {
    @FXML
    private Pagination pagination;
    @FXML
    private Button createNewEventBttn;
    @FXML
    private Button changeTypeBttn;
    private PageService service;
    private User currentUser;
    private int selectedPage;
    private static final int ROWS_PER_PAGE = 3;
    Stage mainStage;
    private int type;
    Stack<SceneType> undoStack;
    private ObservableList<Event> model = FXCollections.observableArrayList();
    @FXML
    private Accordion eventsAccordion;
    @Override
    public void initialize() {
        createNewEventBttn.setTooltip(new Tooltip("Creeaza un eveniment nou."));
        changeTypeBttn.setTooltip(new Tooltip("Vezi evenimentele la care participi."));
        pagination.setCurrentPageIndex(0);

        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedPage = newValue.intValue();
                    if(type == 1)
                        changeAccordionView(newValue.intValue() + 1, ROWS_PER_PAGE);
                    else
                        initMyEvents(newValue.intValue()+1,ROWS_PER_PAGE);
                });

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        service.addObserver(this);
        this.mainStage = mainStage;
        this.currentUser = service.getCurrentUser();
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.AllEvents);
        this.selectedPage = 0;
        type = 1;
        Platform.runLater(()->changeAccordionView(1,ROWS_PER_PAGE));
        int totalPage = (int) (Math.ceil(StreamSupport.stream(service.getEventService().findAll().spliterator(), false).count() * 1.0 / ROWS_PER_PAGE));
        pagination.setPageCount(totalPage);
    }

    private void changeAccordionView(int index, int limit) {
        model.clear();
        eventsAccordion.getPanes().clear();
        model.setAll(service.getEventService().findAllPaging(index,limit));
            for(Event event: model){
                EventPane eventPane = new EventPane(service,event,service.getCurrentUser());
                eventsAccordion.getPanes().add(eventPane.getTitledPane());
            }
    }

    @FXML
    public void goHome() throws IOException {
        MainController ctrl = (MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void createNewEvent(ActionEvent actionEvent) throws IOException {
        CreateNewEventController ctrl = (CreateNewEventController) SceneFactory.changeScene(SceneType.NewEvent,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void initMyEvents(int index,int limit){
        this.model.clear();
        this.eventsAccordion.getPanes().clear();
        model.setAll(service.getEventService().usersParticipatingEvent(index, limit, currentUser.getId()));
        for (Event event : model) {
            EventPane eventPane = new EventPane(service, event, service.getCurrentUser());
            eventsAccordion.getPanes().add(eventPane.getTitledPane());
        }
    }

    public void myEvents(ActionEvent actionEvent) {
        if(type == 1) {
            Platform.runLater(()-> {
                changeTypeBttn.setText("Toate evenimentele");
                changeTypeBttn.setTooltip(new Tooltip("Vezi toate evenimentele."));
                type = 2;
                initMyEvents(1, ROWS_PER_PAGE);
                int totalPage = (int) (Math.ceil(service.getEventService().countMyEvents(currentUser) * 1.0 / ROWS_PER_PAGE));
                pagination.setPageCount(totalPage);
                pagination.setCurrentPageIndex(0);
            });
        }
        else{
            Platform.runLater(()-> {
                type = 1;
                changeTypeBttn.setText("Evenimentele mele");
                changeTypeBttn.setTooltip(new Tooltip("Vezi evenimentele la care participi."));
                changeAccordionView(1, ROWS_PER_PAGE);
                int totalPage = (int) (Math.ceil(StreamSupport.stream(service.getEventService().findAll().spliterator(), false).count() * 1.0 / ROWS_PER_PAGE));
                pagination.setPageCount(totalPage);
                pagination.setCurrentPageIndex(0);
            });
        }
    }

    @Override
    public void update() {
        if (type == 1) {
            changeAccordionView(selectedPage + 1, ROWS_PER_PAGE);
            int totalPage = (int) (Math.ceil(StreamSupport.stream(service.getEventService().findAll().spliterator(), false).count() * 1.0 / ROWS_PER_PAGE));
            pagination.setPageCount(totalPage);
        }
        else {
            initMyEvents(selectedPage+1, ROWS_PER_PAGE);
            int totalPage = (int) (Math.ceil(service.getEventService().countMyEvents(currentUser) * 1.0 / ROWS_PER_PAGE));
            pagination.setPageCount(totalPage);
        }

    }
}
