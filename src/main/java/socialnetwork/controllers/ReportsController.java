package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ReportsController implements Controller {
    PageService service;
    Stack<SceneType> undoStack;
    User connectedUser;
    User user;
    LocalDateTime startDate;
    LocalDateTime finishDate;
    ObservableList<User> model = FXCollections.observableArrayList();
    int reportType;
    @FXML
    Stage mainStage;
    @FXML
    Button generateReport;
    @FXML
    DatePicker datePicker1;
    @FXML
    DatePicker datePicker2;
    @FXML
    TableView<User> userTable;
    @FXML
    TextField selectedUser;
    @FXML
    TableColumn<User,String> firstNameColumn;
    @FXML
    TableColumn<User,String> lastNameColumn;
    @FXML
    private Label userLabel;

    @Override
    public void initialize() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));

        userTable.setItems(model);
        userTable.setRowFactory(t->{
            TableRow<User> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount() >= 1 && (!row.isEmpty())){
                        User u = row.getItem();
                        selectedUser.setText(u.getFirstName()+" "+u.getLastName());
                    }
                }
            });
            return row;
        });
        userTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        firstNameColumn.minWidthProperty().bind(
                userTable.widthProperty().multiply(0.6));
    }

    public void initModel(){
        Iterable<User> users = service.getUserService().getAll();
        List<User> userList = StreamSupport.stream(users.spliterator(),false)
                .filter(x->!x.getId().equals(connectedUser.getId()))
                .collect(Collectors.toList());
        model.setAll(userList);
    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack, int reportType){
        this.service = service;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        this.reportType = reportType;
        this.connectedUser = service.getCurrentUser();
        initModel();
        manageReportType();
    }

    public void goHome(ActionEvent actionEvent) throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }

    public void manageReportType(){
       if(reportType == 1){
           selectedUser.setVisible(false);
           userLabel.setVisible(false);
           userTable.setVisible(false);
        }
    }


    public void generateReport(ActionEvent actionEvent) throws IOException {
        try {
            LocalDate data1 = datePicker1.getValue();
            LocalDate data2 = datePicker2.getValue();
            if (data1 == null)
                throw new ComunityException("Selectati de o data de inceput.");
            if (data2 == null)
                throw new ComunityException("Selectati o data de sfarsit.");
            this.startDate = LocalDateTime.of(data1.getYear(), data1.getMonth(), data1.getDayOfMonth(), 0, 1);
            this.finishDate = LocalDateTime.of(data2.getYear(), data2.getMonth(), data2.getDayOfMonth(), 0, 1);
            service.validateDates(startDate, finishDate);

            GeneratedReportController ctrl = (GeneratedReportController) SceneFactory.changeScene(SceneType.GeneratedReport,mainStage);

            switch (reportType) {
                case 1:
                    try {
                        ctrl.setContext(service,mainStage,undoStack,null,1,startDate,finishDate);
                    } catch (ComunityException e) {
                        CustomMessageWindow.showErrorMessage(null, e.getMessage());
                    }
                    break;

                case 2:
                    this.user = userTable.getSelectionModel().getSelectedItem();
                    try {

                        if (user == null)
                            throw new ComunityException("Alegeti un utilizator.");
                        service.validateDates(startDate, finishDate);
                        ctrl.setContext(service,mainStage,undoStack,user,2,startDate,finishDate);
                    } catch (ComunityException e) {
                        CustomMessageWindow.showErrorMessage(null, e.getMessage());
                    }
                    break;
            }
        } catch (ComunityException e) {
            CustomMessageWindow.showErrorMessage(null, e.getMessage());
        }

    }
}
