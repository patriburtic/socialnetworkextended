package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.Observer.Observer;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.FriendRequestDTO;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;

import java.io.IOException;
import java.util.Stack;

public class FriendRequestController implements Observer {
    @FXML
    private Pagination pagination;
    PageService service;
    private static final int ROWS_PER_PAGE = 4;
    Stage mainStage;
    Stack<SceneType> undoStack;
    User connectedUser;
    int selectedPage;
    ObservableList<FriendRequestDTO> model = FXCollections.observableArrayList();
    @FXML
    TableView<FriendRequestDTO> table;
    @FXML
    TableColumn<FriendRequestDTO,String> usernameColumn;
    @FXML
    TableColumn<FriendRequestDTO,String> dateColumn;
    @FXML
    TableColumn<FriendRequestDTO,String> statusColumn;
    @FXML
    Button acceptBttn;
    @FXML
    Button rejectBttn;

    @FXML
    public void initialize(){
        usernameColumn.setCellValueFactory(new PropertyValueFactory<FriendRequestDTO,String>("username"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<FriendRequestDTO,String>("date"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<FriendRequestDTO,String>("status"));
        table.setItems(model);
        disableButtons();
        pagination.setCurrentPageIndex(0);

        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedPage = newValue.intValue();
                    initModel(newValue.intValue()+1, ROWS_PER_PAGE);
                });
    }

    public void disableButtons(){
        table.setRowFactory(t->{
            TableRow<FriendRequestDTO> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount() >= 1 && (!row.isEmpty())){
                        FriendRequestDTO friendRequestDTO = row.getItem();
                        if(!friendRequestDTO.getStatus().equals("PENDING")){
                            acceptBttn.setDisable(true);
                            rejectBttn.setDisable(true);
                        }
                        else{
                            acceptBttn.setDisable(false);
                            rejectBttn.setDisable(false);
                        }

                    }
                }
            });
            return row;
        });
    }
    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.service.addObserver(this);
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.FriendRequest);
        this.mainStage = mainStage;
        this.selectedPage = 0;
        this.connectedUser = service.getCurrentUser();
        initModel(1,ROWS_PER_PAGE);
        int totalPage = (int) (Math.ceil(service.getFriendRequestService().countFriendRequests(connectedUser) * 1.0 / ROWS_PER_PAGE));
        pagination.setPageCount(totalPage);
    }

    private void initModel(int index, int limit){

        model.setAll(service.getFriendRequestsPaging(index,limit, connectedUser));
    }

    public void goHome() throws IOException {
        MainController ctrl=(MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void acceptFriendRequest() {
        FriendRequestDTO selectedItem = table.getSelectionModel().getSelectedItem();
        if(selectedItem == null){
            CustomMessageWindow.showErrorMessage(null,"Nimic selectat!");
        }
        else {
            try {
                service.respondToFriendRequestByUsername(selectedItem.getUsername(), connectedUser, 1);
            } catch (ComunityException e) {
                CustomMessageWindow.showErrorMessage(null, e.getMessage());
            }
        }
    }

    public void rejectFriendRequest() {
        FriendRequestDTO selectedItem = table.getSelectionModel().getSelectedItem();
        if(selectedItem == null){
            CustomMessageWindow.showErrorMessage(null,"Nimic selectat!");
        }
        else {
            try {
                service.respondToFriendRequestByUsername(selectedItem.getUsername(), connectedUser, 2);
            } catch (ComunityException e) {
                CustomMessageWindow.showErrorMessage(null, e.getMessage());
            }
        }
    }

    @Override
    public void update() {
        initModel(selectedPage+1,ROWS_PER_PAGE);
    }
}
