package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SearchController implements Controller{
    @FXML
    private Pagination pagination;
    private int dataSize;
    private static final int ROWS_PER_PAGE = 4;
    private FilteredList<User> filteredData;
    PageService service;
    User connectedUser;
    Stage mainStage;
    Stack<SceneType> undoStack;
    ObservableList<User> model = FXCollections.observableArrayList();
    @FXML
    TextField searchField;
    @FXML
    TableView<User> table;
    @FXML
    TableColumn<User,String> firstNameColumn;
    @FXML
    TableColumn<User,String> lastNameColumn;
    @FXML
    TableColumn<User,String> usernameColumn;
    @FXML
    public void initialize(){
        filteredData = new FilteredList<>(model, p->true);

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("username"));
        table.setItems(model);
        table.setRowFactory(t->{
            TableRow<User> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount() == 2 && (!row.isEmpty())){
                        User u = row.getItem();
                        try {
                            if(u.equals(connectedUser)){
                                goHome();
                            }
                            else {
                                CustomUserPageController ctrl = (CustomUserPageController) SceneFactory.changeScene(SceneType.CustomUserPage, mainStage);
                                ctrl.setContext(service, mainStage, undoStack, u);
                            }
                        }catch(IOException e){
                            e.printStackTrace();
                        }

                    }
                }
            });
            return row;
        });

        pagination.setCurrentPageIndex(0);

        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableView(newValue.intValue()+1, ROWS_PER_PAGE));

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.Search);
        this.connectedUser = service.getCurrentUser();
        this.mainStage = mainStage;
        int totalPage = (int) (Math.ceil(StreamSupport.stream(service.getUserService().getAll().spliterator(),false).count() * 1.0 / ROWS_PER_PAGE));
        pagination.setPageCount(totalPage);
        changeTableView(1,ROWS_PER_PAGE);

    }

    private void changeTableView(int index, int limit) {
        model.setAll(service.getUserService().findAllPaging(index,limit));
    }


    private List<User> getUserDTOList(){
        return StreamSupport.stream(service.getUserService().getAll().spliterator(), false)
                .filter(x->!x.getId().equals(connectedUser.getId()))
                .collect(Collectors.toList());
    }

    @FXML
    public void goHome() throws IOException {
        MainController ctrl = (MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    @FXML
    public void handleFilter(){
        model.setAll(getUserDTOList().stream()
                .filter(x->x.getFirstName().toLowerCase().startsWith(searchField.getText().toLowerCase())
                        ||x.getLastName().toLowerCase().startsWith(searchField.getText().toLowerCase())
                        ||x.getUsername().toLowerCase().startsWith(searchField.getText().toLowerCase()))
                .collect(Collectors.toList()));

    }

    public void handleUndo(ActionEvent actionEvent) {

    }
}
