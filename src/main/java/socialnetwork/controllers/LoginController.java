package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;

import java.io.IOException;
import java.util.Stack;


public class LoginController implements Controller {
    PageService service;
    Stage loginStage;
    @FXML
    TextField username;
    @FXML
    PasswordField password;
    @FXML
    Button signInButton;
    @FXML
    Hyperlink createNewAccount;

    @FXML
    public void initialize(){

    }

    @FXML
    public void handleSignIn(ActionEvent actionEvent) throws IOException {
        String currentUsername = username.getText();
        String currentPassword = password.getText();
        try {
            service.logIn(currentUsername, currentPassword);
            loginStage.close();
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/homePageView.fxml"));
            BorderPane root=loader.load();
            Stage primaryStage = new Stage();
            primaryStage.setScene(new Scene(root, 700, 500));
            MainController ctrl=loader.getController();
            Stack<SceneType> undoStack = new Stack<>();
            ctrl.setContext(service,primaryStage,undoStack);
            primaryStage.setTitle("Link!");
            primaryStage.getIcons().add(new Image("/images/Hypen.png"));
            primaryStage.show();
        }catch(ComunityException exception){
            CustomMessageWindow.showErrorMessage(null, exception.getMessage());
        }
    }

    public void setContext(PageService service, Stage loginStage){
        this.service = service;
        this.loginStage = loginStage;
    }

    public void createNewAccount(ActionEvent actionEvent) throws IOException {
        loginStage.close();
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/createNewAccount.fxml"));
        BorderPane root=loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root, 700, 500));
        CreateAccountController ctrl=loader.getController();
        Stack<SceneType> undoStack = new Stack<>();
        ctrl.setContext(service,primaryStage);
        primaryStage.setTitle("Creeaza cont nou");
        primaryStage.getIcons().add(new Image("/images/Hypen.png"));
        primaryStage.show();
    }
}
