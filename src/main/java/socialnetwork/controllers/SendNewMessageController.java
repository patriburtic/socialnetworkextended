package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SendNewMessageController implements Controller{
    PageService service;
    Stage mainStage;
    User connectedUser;
    Stack<SceneType> undoStack;
    List<User> toList;
    ObservableList<User> model = FXCollections.observableArrayList();
    Message messageToSend;

    @FXML
    TableColumn<User, String> firstNameColumn;
    @FXML
    TableColumn<User, String> lastNameColumn;
    @FXML
    TableView<User> table;
    @FXML
    TextField toListField;
    @FXML
    TextArea textMessage;
    @Override
    public void initialize() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));
        table.setItems(model);
        table.setRowFactory(t->{
            TableRow<User> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount() >= 1 && (!row.isEmpty())){
                        User u = row.getItem();
                        if(!toList.contains(u)) {
                            toList.add(u);
                            row.setStyle("-fx-background-color: #f9b7a9");
                            messageToSend.setTo(toList);
                            toListField.setText(messageToSend.toListToString());
                        }
                        else{
                            CustomMessageWindow.showErrorMessage(null,"Acest destinatar a fost deja adaugat");
                        }
                    }
                }
            });
            return row;
        });
    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        connectedUser = service.getCurrentUser();
        toList = new ArrayList<>();
        messageToSend = new Message(connectedUser,toList,"");
        initModel();
    }


    public void initModel(){
        Iterable<User> users = service.getUserService().getAll();
        List<User> userList = StreamSupport.stream(users.spliterator(),false)
                .filter(x->!x.getId().equals(connectedUser.getId()))
                .collect(Collectors.toList());
        model.setAll(userList);
    }

    public void goHome() throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }

    public void showTable(MouseEvent mouseEvent) {
        table.setVisible(true);
    }

    public void hideTable(MouseEvent mouseEvent) {
        table.setVisible(false);
    }

    public void sendMessage(ActionEvent actionEvent) {
        if (!(toList.size() > 0)){
            CustomMessageWindow.showErrorMessage(null, "Selectati destinatari!");
        }
        else if(!(textMessage.getText().length() > 0)){
            CustomMessageWindow.showErrorMessage(null, "Mesaj gol!");
        }
        else {
            messageToSend.setDate(LocalDateTime.now());
            messageToSend.setFrom(connectedUser);
            messageToSend.setMessage(textMessage.getText());
            try {
                service.getMessageService().addMessage(messageToSend);
                textMessage.setText("");
                toList.clear();
                toListField.clear();
                CustomMessageWindow.showMessage(null, Alert.AlertType.CONFIRMATION,"Succes", "Mesaj trimis cu succes!");
            }catch(IllegalArgumentException iae){
                CustomMessageWindow.showErrorMessage(null,iae.getMessage());
            }
        }
    }

    public void deleteLastReceiver(ActionEvent actionEvent) {
        toList.clear();
        messageToSend.setTo(toList);
        toListField.setText("");
        table.refresh();
    }
}
