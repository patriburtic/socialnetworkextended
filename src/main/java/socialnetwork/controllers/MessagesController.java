package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.Observer.Observer;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.Message;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Stack;

public class MessagesController implements Observer, Controller {
    public TableColumn<Message, String> usernameColumn;
    public TableColumn<Message, String> dateColumn;
    public TableColumn<Message, String> usernameColumn1;
    public TableColumn<Message, String> dateColumn1;
    int inboxSize;
    int sentMessagesSize;
    PageService service;
    Stage mainStage;
    Stack<SceneType> undoStack;
    User connectedUser;
    Message selectedMessage;
    ObservableList<MessageDTO> model = FXCollections.observableArrayList();
    ObservableList<MessageDTO> modelSent = FXCollections.observableArrayList();
    @FXML
    TableView<MessageDTO> inboxTable;
    @FXML
    TableView<MessageDTO> sentMessagesTable;
    @FXML
    private Pagination pagination;
    private int selectedPage;
    private int tab;
    private static final int ROWS_PER_PAGE = 4;

    @FXML
    BorderPane root;


    @FXML
    public void initialize(){
        inboxTable.setItems(model);
        sentMessagesTable.setItems(modelSent);
        handleRowClick(inboxTable);
        handleRowClick(sentMessagesTable);
        usernameColumn.setCellValueFactory(new PropertyValueFactory<Message,String>("from"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<Message,String>("date"));
        usernameColumn1.setCellValueFactory(new PropertyValueFactory<Message,String>("from"));
        dateColumn1.setCellValueFactory(new PropertyValueFactory<Message,String>("date"));


        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedPage = newValue.intValue();
                    initModel(selectedPage+1,ROWS_PER_PAGE);
                }
        );

    }

    private void handleRowClick(TableView<MessageDTO> sentMessagesTable) {
        sentMessagesTable.setRowFactory(t-> {
            TableRow<MessageDTO> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getClickCount() >= 1 && (!row.isEmpty())){
                        selectedMessage = service.getMessageService().findOne(row.getItem().getId());
                        try {
                            SingleMessageController ctrl = (SingleMessageController) SceneFactory.changeScene(SceneType.SingleMessage, mainStage);
                            ctrl.setContext(service, mainStage, undoStack, selectedMessage);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            return row;
        });
    }

    private void initModel(int index, int limit){
        if(tab == 1) {
            model.setAll(service.getMessageService().inbox(index, limit, connectedUser));
            int totalPage = (int) (Math.ceil(inboxSize * 1.0 / ROWS_PER_PAGE));
            pagination.setPageCount(totalPage);
        }
        else {
            modelSent.setAll(service.getMessageService().sentMessages(index, limit, connectedUser));
            int totalPage = (int) (Math.ceil(sentMessagesSize * 1.0 / ROWS_PER_PAGE));
            pagination.setPageCount(totalPage);
        }
    }

    public void setInboxList(int index, int limit){
        model.setAll(service.getMessageService().inbox(index,limit,connectedUser));
    }

    public void setSentMessages(int index, int limit){
        modelSent.setAll(service.getMessageService().sentMessages(index,limit,connectedUser));
    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.service.addObserver(this);
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.MainMessage);
        this.connectedUser = service.getCurrentUser();
        tab = 1;
        inboxSize = service.getMessageService().countInboxSize(connectedUser);
        sentMessagesSize = service.getMessageService().countSentMessages(connectedUser);
        selectedPage = 0;

        initModel(1,ROWS_PER_PAGE);

    }
    public void goHome() throws IOException {
        MainController ctrl=(MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    @Override
    public void update() {
        initModel(selectedPage+1,ROWS_PER_PAGE);
    }

    public void sendNewMessage(ActionEvent actionEvent) throws IOException {
        SendNewMessageController ctrl = (SendNewMessageController) SceneFactory.changeScene(SceneType.SendNewMessage,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void changeTab1(Event event) {
        this.tab = 1;
        if(pagination != null) {
            pagination.setCurrentPageIndex(0);
            initModel(1,ROWS_PER_PAGE);

        }
    }

    public void changeTab2(Event event) {
        this.tab = 2;
        if(pagination != null) {
            pagination.setCurrentPageIndex(0);
            initModel(1,ROWS_PER_PAGE);
        }
    }
}
