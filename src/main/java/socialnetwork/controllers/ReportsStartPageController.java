package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Stack;

public class ReportsStartPageController implements Controller{
    @FXML
    private Button report2;
    @FXML
    private Button report1;
    private PageService service;
    Stage mainStage;
    Stack<SceneType> undoStack;

    @Override
    public void initialize() {

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.ReportStartPage);
    }

    @FXML
    public void goHome() throws IOException {
        MainController ctrl = (MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void report2(ActionEvent actionEvent) throws IOException {
        ReportsController ctrl = (ReportsController) SceneFactory.changeScene(SceneType.Report,mainStage);
        ctrl.setContext(service,mainStage,undoStack,2);
    }

    public void report1(ActionEvent actionEvent) throws IOException {
        ReportsController ctrl = (ReportsController) SceneFactory.changeScene(SceneType.Report,mainStage);
        ctrl.setContext(service,mainStage,undoStack,1);
    }
}
