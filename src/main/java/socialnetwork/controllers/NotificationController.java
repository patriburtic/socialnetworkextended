package socialnetwork.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class NotificationController implements Controller{
    private Stage mainStage;
    private PageService service;
    private Stack<SceneType> undoStack;
    private User connectedUser;
    private int totalNotifications;
    private ObservableList<Notification> model = FXCollections.observableArrayList();
    private static final int ROWS_PER_PAGE = 4;
    @FXML
    private TableView<Notification> table;
    @FXML
    private TableColumn<Notification,String> messageColumn;
    @FXML
    private TableColumn<Notification, String> dateColumn;
    @FXML
    private Pagination pagination;

    @Override
    public void initialize() {
        messageColumn.setCellValueFactory(new PropertyValueFactory<Notification,String>("message"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<Notification,String>("dateString"));
        table.setItems(model);
        table.setRowFactory(t->{
            TableRow<Notification> row = new TableRow<>();
            row.hoverProperty().addListener((observable -> {
                final Notification notification = row.getItem();
                if(row.isHover() && notification != null){
                    row.setTooltip(new Tooltip(notification.getMessage()));
                }
            }));
            return row;
        });

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        messageColumn.minWidthProperty().bind(
                table.widthProperty().multiply(0.55));

        pagination.setCurrentPageIndex(0);

        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    initModel(newValue.intValue()+1,ROWS_PER_PAGE);
                });



    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.connectedUser = service.getCurrentUser();
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        initModel(1,ROWS_PER_PAGE);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                totalNotifications = service.getNotificationService().getAllUserNotifications(connectedUser.getId()).size();
                int totalPage = (int) (Math.ceil(totalNotifications * 1.0 / ROWS_PER_PAGE));
                if(totalPage == 0)
                    pagination.setPageCount(1);
                else
                    pagination.setPageCount(totalPage);
            }
        });
        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setProgress(-1);
        table.setPlaceholder(progressIndicator);
        service.setNewNotification(false);
    }

    public void goHome() throws IOException {
        MainController ctrl=(MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    public void initModel(int index, int limit){
        Thread thread1 = new Thread(()-> {
            List<Notification> notifications = service.getNotificationService().getAllUserNotificationsPaged(new PageableImplementation(index, limit), connectedUser.getId()).getContent().sorted(new Comparator<Notification>() {
                @Override
                public int compare(Notification o1, Notification o2) {
                    if(o1.getDate().isAfter(o2.getDate()))
                        return -1;
                    else if(o1.getDate().isEqual(o2.getDate()))
                        return 0;
                    else
                        return 1;
                }
            }).collect(Collectors.toList());
            model.setAll(notifications);
        });
        thread1.start();
    }
}
