package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MainController implements Controller{
    public Button notificationsBttn;
    ObservableList<User> model = FXCollections.observableArrayList();
    PageService service;
    Stack<SceneType> undoStack;
    User connectedUser;
    @FXML
    Stage stage;
    @FXML
    Label username;

    @FXML
    public void initialize(){

    }
    public void setContext(PageService srv, Stage stage, Stack<SceneType> undoStack){
        this.service = srv;
        this.stage = stage;
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.Main);
        connectedUser = service.getCurrentUser();
        username.setText(connectedUser.getFirstName() + " " + connectedUser.getLastName());
        if(service.isNewNotification())
            notificationsBttn.setStyle("-fx-border-radius: 50; -fx-border-color: red; -fx-border-width: 1.5");
        initModel();
    }

    private void initModel(){
        Iterable<User> users = service.getUserService().getAll();
        List<User> userList = StreamSupport.stream(users.spliterator(),false)
                .filter(x->!x.getId().equals(connectedUser.getId()))
                .collect(Collectors.toList());
        model.setAll(userList);
    }

    public void handleSearchFriendsBttn() throws IOException {
        SearchController ctrl= (SearchController) SceneFactory.changeScene(SceneType.Search,stage);
        ctrl.setContext(service,stage,undoStack);
    }

    @FXML
    public void signOut() throws IOException {
        stage.close();
        FXMLLoader loaderLogin = new FXMLLoader();
        loaderLogin.setLocation(getClass().getResource("/view/loginViewNew.fxml"));
        Pane loginRoot = loaderLogin.load();
        LoginController loginController = loaderLogin.getController();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(loginRoot, 600, 400));
        primaryStage.setTitle("Bun venit!");
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setMaxWidth(600);
        loginController.setContext(service, primaryStage);
        primaryStage.getIcons().add(new Image("/images/Hypen.png"));
        primaryStage.show();
    }

    public void showMyFriends() throws IOException {
        MyFriendsController ctrl=(MyFriendsController) SceneFactory.changeScene(SceneType.MyFriends,stage);
        ctrl.setContext(service,stage,undoStack);
    }

    public void showMyFriendRequests() throws IOException {
        FriendRequestController ctrl=(FriendRequestController) SceneFactory.changeScene(SceneType.FriendRequest,stage);
        ctrl.setContext(service,stage, undoStack);
    }

    public void sendNewMessage(ActionEvent actionEvent) throws IOException {
        SendNewMessageController ctrl = (SendNewMessageController) SceneFactory.changeScene(SceneType.SendNewMessage,stage);
        ctrl.setContext(service,stage,undoStack);
    }

    public void showMessages(ActionEvent actionEvent) throws IOException {
        MessagesController ctrl = (MessagesController) SceneFactory.changeScene(SceneType.MainMessage,stage);
        ctrl.setContext(service,stage,undoStack);

    }

    public void showRaportOptions(ActionEvent actionEvent) throws IOException {
        ReportsStartPageController ctrl = (ReportsStartPageController) SceneFactory.changeScene(SceneType.ReportStartPage,stage);
        ctrl.setContext(service,stage,undoStack);
    }

    public void goToEvents(ActionEvent actionEvent) throws IOException {
        EventController ctrl = (EventController) SceneFactory.changeScene(SceneType.AllEvents,stage);
        ctrl.setContext(service,stage,undoStack);
    }

    public void openNotifications(ActionEvent actionEvent) throws IOException {
        NotificationController ctrl = (NotificationController) SceneFactory.changeScene(SceneType.Notifications,stage);
        ctrl.setContext(service,stage,undoStack);
    }
}
