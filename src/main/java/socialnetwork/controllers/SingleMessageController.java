package socialnetwork.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.BackFactory;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;
import socialnetwork.service.exceptions.ComunityException;
import utils.Constants;

import java.io.IOException;
import java.util.Stack;

public class SingleMessageController implements Controller{
    PageService service;
    Message selectedMessage;
    User connectedUser;
    Stage mainStage;
    Stack<SceneType> undoStack;
    @FXML
    Text from;
    @FXML
    Text to;
    @FXML
    Text date;
    @FXML
    Text message;
    @FXML
    Hyperlink originalMessage;
    @FXML
    TextArea replyArea;
    @FXML
    Button sendButton;
    @FXML
    Label responseLabel;
    @Override
    public void initialize() {

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack, Message selectedMessage){
        this.service = service;
        this.undoStack = undoStack;
        this.mainStage = mainStage;
        this.selectedMessage = selectedMessage;
        this.connectedUser = service.getCurrentUser();
        setData(selectedMessage);
    }

    public void setData(Message currentMessage){
        from.setText(currentMessage.getFrom().getFirstName()+" "+currentMessage.getFrom().getLastName());
        to.setText(currentMessage.toListToString());
        date.setText(currentMessage.getDate().format(Constants.DATE_FORMATTER));
        message.setText(currentMessage.getMessage());
        if(selectedMessage.getReplyTo() ==  null)
            originalMessage.setDisable(true);
        if(selectedMessage.getFrom().equals(connectedUser)) {
            replyArea.setVisible(false);
            sendButton.setVisible(false);
            responseLabel.setVisible(false);
        }
        else{
            replyArea.setVisible(true);
            sendButton.setVisible(true);
            responseLabel.setVisible(true);
        }
    }

    public void showOriginalMessage(ActionEvent actionEvent) {
        selectedMessage = selectedMessage.getReplyTo();
        setData(selectedMessage);
    }

    public void sendReply(ActionEvent actionEvent) {
        if(replyArea.getText().length() > 0){
            try{
                service.replyToMessage(selectedMessage,connectedUser,replyArea.getText());
                CustomMessageWindow.showMessage(null, Alert.AlertType.INFORMATION, "Succes!", "YAY! Mesajul a fost trimis cu succes!");
                replyArea.setText("");
            }catch(ComunityException e){
                CustomMessageWindow.showErrorMessage(null,e.getMessage());
            }
        }
    }

    public void goHome() throws IOException {
        BackFactory.goBack(service,mainStage,undoStack);
    }

    public void goBack(ActionEvent actionEvent) throws IOException {
        MessagesController ctrl = (MessagesController) SceneFactory.changeScene(SceneType.MainMessage,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }
}
