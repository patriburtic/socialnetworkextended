package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.controllers.tools.SceneFactory;
import socialnetwork.controllers.tools.SceneType;
import socialnetwork.domain.FriendDTO;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class MyFriendsController implements Controller{
    public Pagination pagination;
    PageService service;
    Stage mainStage;
    Stack<SceneType> undoStack;
    private static final int ROWS_PER_PAGE = 4;
    User connectedUser;
    int totalFriends;
    ObservableList<FriendDTO> model = FXCollections.observableArrayList();
    @FXML
    TextField searchField;
    @FXML
    TableView<FriendDTO> table;
    @FXML
    TableColumn<FriendDTO,String> firstNameColumn;
    @FXML
    TableColumn<FriendDTO,String> lastNameColumn;
    @FXML
    TableColumn<FriendDTO,String> usernameColumn;
    @FXML
    TableColumn<FriendDTO,String> dateColumn;

    @FXML
    public void initialize(){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<FriendDTO,String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<FriendDTO,String>("lastName"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<FriendDTO,String>("username"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<FriendDTO,String>("date"));
        table.setItems(model);
        table.setRowFactory(t->{
            TableRow<FriendDTO> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount() == 2 && (!row.isEmpty())){
                        FriendDTO friendDTO = row.getItem();
                        User u = service.getUserService().findByUsername(friendDTO.getUsername());
                        try {
                            CustomUserPageController ctrl = (CustomUserPageController) SceneFactory.changeScene(SceneType.CustomUserPage, mainStage);
                            ctrl.setContext(service, mainStage, undoStack, u);
                        }catch (IOException e){
                            e.printStackTrace();
                        }

                    }
                }
            });
            return row;
        });

        pagination.setCurrentPageIndex(0);

        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    initModel(newValue.intValue()+1,ROWS_PER_PAGE);
                });

    }

    public void setContext(PageService service, Stage mainStage, Stack<SceneType> undoStack){
        this.service = service;
        this.mainStage = mainStage;
        this.undoStack = undoStack;
        this.undoStack.push(SceneType.MyFriends);
        this.connectedUser = service.getCurrentUser();
        initModel(1,ROWS_PER_PAGE);
        totalFriends = service.getUserFriendsDTO(connectedUser).size();
        int totalPage = (int) (Math.ceil(totalFriends * 1.0 / ROWS_PER_PAGE));
        pagination.setPageCount(totalPage);
    }
    private void initModel(int index, int limit){
        List<FriendDTO> users = service.getUserFriends(index,limit,connectedUser);
        model.setAll(users);
    }

    public void goHome() throws IOException {
        MainController ctrl=(MainController) SceneFactory.changeScene(SceneType.Main,mainStage);
        ctrl.setContext(service,mainStage,undoStack);
    }

    private List<FriendDTO> getUserDTOList(){
        return new ArrayList<>(service.getUserFriendsDTO(connectedUser));
    }

    public void handleFilter(KeyEvent keyEvent) {
        model.setAll(getUserDTOList().stream()
                .filter(x->x.getFirstName().toLowerCase().startsWith(searchField.getText().toLowerCase())
                        ||x.getLastName().toLowerCase().startsWith(searchField.getText().toLowerCase())
                        ||x.getUsername().toLowerCase().startsWith(searchField.getText().toLowerCase()))
                .collect(Collectors.toList()));
    }
}
