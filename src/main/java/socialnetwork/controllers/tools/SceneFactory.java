package socialnetwork.controllers.tools;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * SceneFactory is a tool through which the scenes are easily changed in the application based on the SceneType
 */
public class SceneFactory {

    /**
     * static method
     * @param sceneType - the scene to be shown
     * @param stage - the main stage
     * @return Object which represents the specific controller, based on the scene type given (need to be cast)
     * @throws IOException -
     */
    public static Object changeScene(SceneType sceneType, Stage stage) throws IOException {
        FXMLLoader loader=new FXMLLoader();
        switch (sceneType) {
            case Main -> loader.setLocation(SceneFactory.class.getResource("/view/homePageView.fxml"));
            case CustomUserPage -> loader.setLocation(SceneFactory.class.getResource("/view/customUserPage.fxml"));
            case FriendRequest -> loader.setLocation(SceneFactory.class.getResource("/view/myFriendRequests.fxml"));
            case MyFriends -> loader.setLocation(SceneFactory.class.getResource("/view/myFriendsView.fxml"));
            case Search -> loader.setLocation(SceneFactory.class.getResource("/view/searchFriendsView.fxml"));
            case MainMessage -> loader.setLocation(SceneFactory.class.getResource("/view/mainMessagePageView.fxml"));
            case SendNewMessage -> loader.setLocation(SceneFactory.class.getResource("/view/sendNewMessageView.fxml"));
            case ReportStartPage -> loader.setLocation(SceneFactory.class.getResource("/view/reportsStartPage.fxml"));
            case GeneratedReport -> loader.setLocation(SceneFactory.class.getResource("/view/generatedReport.fxml"));
            case AllEvents -> loader.setLocation(SceneFactory.class.getResource("/view/eventsView.fxml"));
            case NewEvent -> loader.setLocation(SceneFactory.class.getResource("/view/createNewEventView.fxml"));
            case Report -> loader.setLocation(SceneFactory.class.getResource("/view/reportsNew.fxml"));
            case SingleMessage -> loader.setLocation(SceneFactory.class.getResource("/view/SingleMessageView.fxml"));
            case Notifications -> loader.setLocation(SceneFactory.class.getResource("/view/notificationsView.fxml"));
        }
        BorderPane root=loader.load();
        stage.setScene(new Scene(root, 700, 500));
        return loader.getController();
    }


}
