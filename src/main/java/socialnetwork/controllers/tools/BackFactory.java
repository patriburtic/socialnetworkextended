package socialnetwork.controllers.tools;

import javafx.stage.Stage;
import socialnetwork.controllers.*;
import socialnetwork.service.PageService;

import java.io.IOException;
import java.util.Stack;

/**
 * BackFactory is a class which helps us to move back to the precedent scene based on the last SceneType introduced in the undoStack
 */
public class BackFactory {
    /**
     * static method
     * the following parameters are needed to set the Context for each Controller
     * @param service - the CommunityService
     * @param mainStage - the main stage
     * @param undoStack - stack of scene types
     * @throws IOException -
     */
    public static void goBack(PageService service, Stage mainStage, Stack<SceneType> undoStack) throws IOException {
        SceneType sceneType = undoStack.pop();
        Object object = SceneFactory.changeScene(sceneType,mainStage);
        switch(sceneType){
            case Main -> {
                MainController controller = (MainController) object;
                controller.setContext(service,mainStage,undoStack);
            }
            case FriendRequest -> {
                FriendRequestController controller = (FriendRequestController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case MyFriends -> {
                MyFriendsController controller = (MyFriendsController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case Search -> {
                SearchController controller = (SearchController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case MainMessage -> {
                MessagesController controller = (MessagesController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case SendNewMessage -> {
                SendNewMessageController controller = (SendNewMessageController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case ReportStartPage -> {
                ReportsStartPageController controller = (ReportsStartPageController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case AllEvents -> {
                EventController controller = (EventController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case NewEvent -> {
                CreateNewEventController controller = (CreateNewEventController) object;
                controller.setContext(service, mainStage, undoStack);
            }
            case Notifications -> {
                NotificationController controller = (NotificationController) object;
                controller.setContext(service, mainStage, undoStack);
            }
        }

    }
}
