package socialnetwork.controllers.tools;

/**
 * Scenes used in this application
 * {@link #Main}
 * {@link #CustomUserPage}
 * {@link #MyFriends}
 * {@link #Search}
 * {@link #MainMessage}
 * {@link #SendNewMessage}
 * {@link #Report}
 * {@link #GeneratedReport}
 * {@link #AllEvents}
 * {@link #NewEvent}
 * {@link #ReportStartPage}
 * {@link #SingleMessage}
 * {@link #Notifications}
 */

public enum SceneType {
    /**
     * Main is the main Scene, which contains information about the current user and different features
     */
    Main,
    /**
     * CustomUserPage is a Scene which contains information about a user (different from the current one)
     * Through this Scene, the current user is able to send a friend request to another user
     * and delete the friendships (if they are already friends)
     */
    CustomUserPage,
    /**
     * FriendRequest is a Scene which contains all the friend requests sent to the current user
     * Through this scene, the current user can accept or reject a friend request
     */
    FriendRequest,
    /**
     * MyFriends contains information about current user's friends (First name, last name, user name and the date since they are friends
     */
    MyFriends,
    /**
     * Through this scene, the current user is able to search other users and visit their profile
     */
    Search,
    /**
     * This Scene is a mail - like interface, through which the current user is able to see all the received message and respond to them, and sent messages, as well
     */
    MainMessage,
    /**
     * Through this scene, the current user is able to send a new message, without responding to an existing message
     */
    SendNewMessage,
    /**
     * Report Scene allows the current user to select dates and users, based on which the reports will be generated
     */
    Report,
    /**
     * GeneratedReport is a scene containing a generated report based on current user's choices
     */
    GeneratedReport,
    /**
     * This scene contains information about all the existing events, and other features, such as creating a new event
     */
    AllEvents,
    /**
     * NewEvent contains a form based on which a new event can be created
     */
    NewEvent,
    /**
     * ReportStartPage is a simple interface through which the user can choose what kind of report he wants to generate
     */
    ReportStartPage,
    /**
     * SingleMessage Scene contains information about a sent or received message
     */
    SingleMessage,
    /**
     * Notifications Scene contains all the user's notifications regarding the events he is interested in
     */
    Notifications
}
