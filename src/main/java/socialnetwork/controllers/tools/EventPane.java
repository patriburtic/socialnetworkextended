package socialnetwork.controllers.tools;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.service.PageService;

import java.time.LocalDateTime;

public class EventPane extends TitledPane {
    private Event event;
    private PageService service;
    private User currentUser;
    private boolean participating;
    private boolean receivesNotification;
    @FXML
    private VBox vBox;
    @FXML
    private Label title;
    @FXML
    private Label location;
    @FXML
    private Label period;
    @FXML
    private Text description;
    @FXML
    private Button participateButton;
    @FXML
    private Button notificationButton;
    @FXML
    TitledPane titledPane;
    @FXML
    Label completeName;

    public EventPane(PageService service, Event e, User currentUser) {
        this.event = e;
        this.currentUser = currentUser;
        this.service = service;
        this.title= new Label("Titlu: "+e.getTitle());
        this.location = new Label("Locatie: "+e.getLocation());
        this.period = new Label("Perioada: "+e.getPeriod());
        this.completeName = new Label("Initiator: "+e.getCreator().getFirstName()+" "+e.getCreator().getLastName());
        this.description = new Text("Descriere: "+e.getDescription());
        this.description.setFont(Font.font("Calibri",25));
        this.description.setWrappingWidth(600);
        participating = service.getEventService().findOne(event.getId()).getParticipantsOnly().contains(currentUser);
        notificationButton = new Button("Opreste notificari");
        if(!participating){
            participateButton = new Button("Participa");
            participateButton.setTooltip(new Tooltip("Participa la eveniment"));
        }else{
            participateButton = new Button("Nu mai particip");
            participateButton.setTooltip(new Tooltip("Momentan participati la acest eveniment. Pentru a anula participarea, apasati butonul."));
        }

        manageBttn();

        participateButton.setOnAction(event -> {
            if(!participating) {
                service.addParticipantToEvent(e.getId(), currentUser.getId());
                    Notifications notification = Notifications.create()
                            .position(Pos.TOP_CENTER)
                            .hideAfter(Duration.seconds(2))
                            .title("SUCCES")
                            .text("Participati la evenimentul \" " + e.getTitle() + "\".");
                    notification.show();
            }
            else{
                service.removeParticipantFromEvent(e.getId(),currentUser.getId());
                Notifications notification = Notifications.create()
                        .position(Pos.TOP_CENTER)
                        .hideAfter(Duration.seconds(2))
                        .title("Anulare participare")
                        .text("Nu mai participati la evenimentul \" " + e.getTitle() + "\".");
                notification.show();

            }
            manageBttn();
        });

        notificationButton.setOnAction(event -> {
            receivesNotification = service.getEventService().findOne(e.getId()).getParticipants().stream().anyMatch(x -> x.getLeft().equals(currentUser) && x.getRight().equals(true));
            if(!receivesNotification){
                service.updateUserChoice(e, currentUser, true);
                receivesNotification = service.getEventService().findOne(e.getId()).getParticipants().stream().anyMatch(x -> x.getLeft().equals(currentUser) && x.getRight().equals(true));
                Notifications notification = Notifications.create()
                        .position(Pos.TOP_CENTER)
                        .hideAfter(Duration.seconds(3))
                        .title("Notificari pornite")
                        .text("Veti primi notificari despre evenimentul \" " + e.getTitle() + "\".");
                notification.show();
            }
            else{
                service.updateUserChoice(e, currentUser, false);
                receivesNotification = service.getEventService().findOne(e.getId()).getParticipants().stream().anyMatch(x -> x.getLeft().equals(currentUser) && x.getRight().equals(true));
                Notifications notification = Notifications.create()
                        .position(Pos.TOP_CENTER)
                        .hideAfter(Duration.seconds(3))
                        .title("Notificari oprite")
                        .text("Nu veti mai primi notificari legate de evenimentul \" " + e.getTitle() + "\".");
                notification.show();
            }
            manageBttn();

        });
        vBox = new VBox();
        Label label = new Label("Acest eveniment a expirat.");
        label.setStyle("-fx-text-fill: red");
        if(LocalDateTime.now().isAfter(event.getFinishDate())){
            vBox.getChildren().addAll(title,location,period,description,completeName,label);
        }
        else {
            vBox.getChildren().addAll(title, location, period, description,completeName, participateButton, notificationButton);
        }
        vBox.setSpacing(2);
        if(!participating)
            notificationButton.setVisible(false);
        titledPane = new TitledPane(e.getTitle(),vBox);
    }

    public TitledPane getTitledPane() {
        return titledPane;
    }

    public void manageBttn(){
        participating = service.getEventService().findOne(event.getId()).getParticipantsOnly().contains(currentUser);
        receivesNotification = service.getEventService().findOne(event.getId()).getParticipants().stream().anyMatch(x -> x.getLeft().equals(currentUser) && x.getRight().equals(true));

        if(!participating){
            notificationButton.setVisible(false);
            participateButton.setText("Participa");
            participateButton.setTooltip(new Tooltip("Participa la eveniment"));
        }else{
            notificationButton.setVisible(true);
            if(receivesNotification)
                notificationButton.setText("Opreste notificari");
            else
                notificationButton.setText("Porneste notificari");
            participateButton.setText("Nu mai particip");
            participateButton.setTooltip(new Tooltip("Momentan participati la acest eveniment. Pentru a anula participarea, apasati butonul."));
        }
    }

}
