package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.domain.utils.Status;
import socialnetwork.service.PageService;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.exceptions.ComunityException;
import utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UI {
    private PageService comunityService;

    public UI(PageService comunityService) {
        this.comunityService = comunityService;
    }

    public void run() throws IOException {
        int command;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            System.out.println("Meniu:");
            System.out.println("0. Exit");
            System.out.println("1. Adauga utilizator");
            System.out.println("2. Sterge utilizator");
            System.out.println("3. Adauga prietenie");
            System.out.println("4. Sterge prietenie");
            System.out.println("5. Numar de comunitati");
            System.out.println("6. Cea mai sociabila comunitate");
            System.out.println("7. Afiseaza toti prietenii unui utilizator");
            System.out.println("8. Afiseaza toti prietenii unui utilizator dupa luna");
            System.out.println("9. Trimite mesaj");
            System.out.println("10. Raspunde la mesaj");
            System.out.println("11. Afiseaza utilizatori");
            System.out.println("12. Afiseaza conversatia dintre 2 utilizatori");
            System.out.println("13. Trimite cerere de prietenie");
            System.out.println("14. Raspunde la cererile de prietenie");
            System.out.println("Comanda: ");
            command = Integer.parseInt(bufferedReader.readLine());
            switch (command) {
                case 0:
                    return;
                case 1:
                    addUser();
                    break;
                case 2:
                    deleteUser();
                    break;
                case 3:
                    addPrietenie();
                    break;
                case 4:
                    deletePrietenie();
                    break;
                case 5:
                    System.out.println("Exista " + comunityService.nrComunitati() +" comunitati.");
                    break;
                case 6:
                    System.out.println("Cea mai sociabila comunitate: " + comunityService.most_sociable_comunity());
                    break;
                case 7:
                    showAllFriends();
                    break;
                case 8:
                    showFriendsByMonth();
                    break;
                case 9:
                    sendMessage();
                    break;
                case 10:
                    replyToMessage();
                    break;
                case 11:
                    printAll();
                    break;
                case 12:
                    showConversations();
                    break;
                case 13:
                    sendFriendRequest();
                    break;
                case 14:
                    respondToPendingFriendRequestsForUser();
                    break;
                default:
                    System.out.println("Comanda invalida.");
                    break;

            }
        }

    }

    private void addUser() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Prenume:");
        String firstName = bufferedReader.readLine();
        System.out.println("Nume:");
        String lastName = bufferedReader.readLine();
        System.out.println("Username:");
        String username = bufferedReader.readLine();
        System.out.println("Parola:");
        String password = bufferedReader.readLine();
        try{
            comunityService.addUser(firstName, lastName, username, password);
            System.out.println("Utilizator adaugat.");
        }catch(ValidationException | IllegalArgumentException | UIException | ComunityException e){
            System.out.println("Exceptie: " + e.getMessage());
        }

    }

    private void deleteUser() throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("ID:");
        String id = bufferedReader.readLine();
        try{
            long longId= Long.parseLong(id);
            comunityService.deleteUser(longId);
            System.out.println("Utilizator sters.");
        }catch(IllegalArgumentException | UIException | ComunityException iae) {
            System.out.println("Exceptie: " + iae.getMessage());
        }
    }

    private void addPrietenie() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("ID 1:");
        String id1 = bufferedReader.readLine();
        System.out.println("ID 2:");
        String id2 = bufferedReader.readLine();
        try{
            long longId1= Long.parseLong(id1);
            long longId2= Long.parseLong(id2);
            comunityService.addFriendship(longId1, longId2);
            System.out.println("Prietenie adaugata.");
        }catch(ValidationException | IllegalArgumentException | UIException | ComunityException e){
            System.out.println("Exceptie: " + e.getMessage());
        }
    }

    private void deletePrietenie() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("ID 1:");
        String id1 = bufferedReader.readLine();
        System.out.println("ID 2:");
        String id2 = bufferedReader.readLine();
        try {
            long longId1 = Long.parseLong(id1);
            long longId2 = Long.parseLong(id2);
            comunityService.deleteFriendship(longId1, longId2);
            System.out.println("Prietenie stearsa.");

        }catch(IllegalArgumentException | UIException | ComunityException iae) {
            System.out.println("Exceptie: " + iae.getMessage());
        }
    }

    private void printAll(){
        comunityService.getUserService().getAll().forEach(System.out::println);
    }

    public void showAllFriends() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("ID:");
        String id1 = bufferedReader.readLine();
        try {
            long longId1 = Long.parseLong(id1);
            List<String> friends = comunityService.showAllFriends(longId1);
            if(friends.size() != 0)
                friends.forEach(System.out::println);
            else
                throw new UIException("Acest user nu are prieteni.");
        }catch(IllegalArgumentException | UIException |ComunityException e){
            System.out.println(e.getMessage());
        }
    }

    private void showFriendsByMonth() throws IOException {
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID:");
            String id1 = bufferedReader.readLine();
            long longId1 = Long.parseLong(id1);
            if(!comunityService.userExists(longId1))
                throw new UIException("Acest user nu exista.");
            System.out.println("Alegeti luna:");
            System.out.println("1.Ianuarie");
            System.out.println("2.Februarie");
            System.out.println("3.Martie");
            System.out.println("4.Aprilie");
            System.out.println("5.Mai");
            System.out.println("6.Iunie");
            System.out.println("7.Iulie");
            System.out.println("8.August");
            System.out.println("9.Septembrie");
            System.out.println("10.Octombrie");
            System.out.println("11.Noiembrie");
            System.out.println("12.Decembrie");

            String month = bufferedReader.readLine();
            int monthInt = Integer.parseInt(month);
            if(monthInt<1 || monthInt>12)
                throw new UIException("Luna invalida.");

            List<String> friends = comunityService.filterFriendsByMonth(longId1, monthInt);
            if(friends.size() != 0)
                friends.forEach(System.out::println);
            else
                throw new UIException("Acest user nu are prieteni din aceasta luna");

        }catch(IllegalArgumentException | UIException |ComunityException iae){
            System.out.println(iae.getMessage());
        }

    }

    private void sendMessage() throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID expeditor:");
            String fromIDS = bufferedReader.readLine();
            long fromID = Long.parseLong(fromIDS);
            if(!comunityService.userExists(fromID))
                throw new UIException("Acest user nu exista!\n");
            User from = comunityService.getUserService().findOne(fromID);
            System.out.println("Numar destinatari:");
            String nrToS = bufferedReader.readLine();
            int nrTo = Integer.parseInt(nrToS);
            if(nrTo < 1)
                throw new UIException("Numarul de destinatari nu trebuie sa fie minim 1.");
            List<Long> to = new ArrayList<>();
            for(int i=1; i<= nrTo; i++){
                System.out.println("ID destinatar "+i+":");
                String toIdS = bufferedReader.readLine();
                long toID = Long.parseLong(toIdS);
                if(!comunityService.userExists(toID))
                    throw new UIException("Acest user nu exista!\n");
                to.add(toID);
            }
            System.out.println("Mesaj:");
            String txt = bufferedReader.readLine();

            comunityService.sendMessage(from,to,txt);
            System.out.println("Mesaj trimis.");

        }catch(IllegalArgumentException | UIException |ValidationException | ComunityException iae){
            System.out.println(iae.getMessage());
        }
    }

    private void replyToMessage() throws IOException {
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID mesaj:");
            long messageId = Long.parseLong(bufferedReader.readLine());
            Message messageToReply = comunityService.getMessageService().findOne(messageId);
            if(messageToReply == null)
                throw new UIException("Nu exista un mesaj cu acest ID!");
            System.out.println("ID expeditor:");
            long fromId = Long.parseLong(bufferedReader.readLine());
            User from = comunityService.getUserService().findOne(fromId);
            if(from == null)
                throw new UIException("Nu exista un user cu acest ID!");
            if(!messageToReply.getTo().contains(from))
                throw new UIException("Acest mesaj nu va este adresat. Nu puteti raspune.");

            System.out.println("Raspuns:");
            String txt = bufferedReader.readLine();
            comunityService.replyToMessage(messageToReply,from, txt);
            System.out.println("Raspuns trimis.");

        }catch(UIException|IllegalArgumentException|ValidationException |ComunityException e){
            System.out.println(e.getMessage());
        }
    }

    private void showConversations() throws IOException {
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID 1:");
            long id1 = Long.parseLong(bufferedReader.readLine());
            User user1 = comunityService.getUserService().findOne(id1);
            if(user1 == null)
                throw new UIException("Nu exista un user cu acest ID!");
            System.out.println("ID 2:");
            long id2 = Long.parseLong(bufferedReader.readLine());
            User user2 = comunityService.getUserService().findOne(id2);
            if(user2 == null)
                throw new UIException("Nu exista un user cu acest ID!");
            List<Message> messages = comunityService.getMessageService().showConversation(user1, user2);
            if (messages.size() == 0)
                throw new UIException("Nu exista mesaje intre acesti utilizatori");
            messages.forEach(x->{
                System.out.println(x);
                if(x.getReplyTo()!= null)
                    System.out.println("Original message: "+ x.getReplyTo());
            });
        }catch(IllegalArgumentException|UIException e){
            System.out.println(e.getMessage());
        }
    }


    private void sendFriendRequest() throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID-ul meu:");
            long myId = Long.parseLong(bufferedReader.readLine());
            User me = comunityService.getUserService().findOne(myId);
            if(me == null)
                throw new UIException("Nu exista un user cu acest ID!");
            System.out.println("Trimite cerere de prietenie la ID-ul:");
            long friendId = Long.parseLong(bufferedReader.readLine());
            User friend = comunityService.getUserService().findOne(friendId);
            if(friend == null)
                throw new UIException("Nu exista un user cu acest ID!");
            comunityService.sendFriendRequest(me, friend);
            System.out.println("Cerere de prietenie trimisa");
        }catch(IllegalArgumentException|UIException| ComunityException |ValidationException e){
            System.out.println(e.getMessage());
        }
    }



    private void respondToPendingFriendRequestsForUser() throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID-ul meu:");
            long myId = Long.parseLong(bufferedReader.readLine());
            User me = comunityService.getUserService().findOne(myId);
            if (me == null)
                throw new UIException("Nu exista un user cu acest ID!");
            List<FriendRequest> myFriendRequests = comunityService.getFriendRequestService().loadPendingRequests(me.getId());
            if(myFriendRequests.size() == 0)
                throw new UIException("Nu exista cereri de prietenie neacceptate!");
            for(FriendRequest x:myFriendRequests){
                User friend = comunityService.getUserService().findOne(x.getId().getLeft());
                System.out.print(friend.getFirstName()+" "+friend.getLastName()+" cu ID = "+friend.getId());
                System.out.print(" v-a trimis o cerere de prietenie la data de ");
                System.out.print(x.getDate().format(Constants.DATE_TIME_FORMATTER));
                System.out.print(". Status: " + x.getStatus());
                System.out.println();
            }
            while(myFriendRequests.size() > 0) {
                System.out.println("Doriti sa raspundeti la o cerere de prietenie?[y/n]");
                String answer = bufferedReader.readLine();
                if(answer.equals("y")) {
                    System.out.println("Dati ID-ul userului la a carui cerere doriti sa raspundeti:");
                    long requestId = Long.parseLong(bufferedReader.readLine());
                    FriendRequest searchRequest = comunityService.getFriendRequestService().findOne(new Tuple<Long, Long>(requestId, myId));
                    if (searchRequest == null)
                        throw new UIException(comunityService.getUserService().findOne(requestId) + " nu v-a trimis cerere de prietenie.");
                    else if (searchRequest.getStatus() != Status.PENDING)
                        throw new UIException("Aceasta cerere nu se afla in stare de asteptare.");
                    else {
                        System.out.println("1.Accept");
                        System.out.println("2.Refuz");
                        int choice = Integer.parseInt(bufferedReader.readLine());
                        if(!(choice == 1 || choice ==2 ))
                            throw new UIException("Comanda invalida!");
                        comunityService.respondToFriendRequest(myId,requestId,choice);
                        switch(choice){
                            case 1:
                                System.out.println("Cerere de prietenie acceptata.");
                                break;
                            case 2:
                                System.out.println("Cerere de prietenie refuzata.");
                                break;
                            default:
                                break;
                        }
                        myFriendRequests = comunityService.getFriendRequestService().loadPendingRequests(me.getId());



                    }
                }
                else if(answer.equals("n"))
                    return;
                else
                    System.out.println("Va rugam sa raspundeti cu [y/n].");
            }

        }catch(IllegalArgumentException|UIException| ComunityException |ValidationException e){
            System.out.println(e.getMessage());
        }
    }
}
