package socialnetwork.Observer;

public interface Observer {
    void update();
}
